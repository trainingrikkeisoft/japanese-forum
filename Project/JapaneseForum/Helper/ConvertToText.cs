﻿using JapaneseForum.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Helper
{
    public class ConvertToText
    {
        public static string ConvertStatusToText(StatusEnum status)
        {
            if (status == StatusEnum.Active)
                return "Kích hoạt";
            else if (status == StatusEnum.Draft)
                return "Nháp";
            else
                return "Không kích hoạt";
        }

        public static string ConvertStatusToText(int status)
        {
            if (status == (int)StatusEnum.Active)
                return "Kích hoạt";
            else if (status == (int)StatusEnum.Draft)
                return "Nháp";
            else
                return "Không kích hoạt";
        }

        public static string ConvertAnswerToText(int anwer)
        {
            var rightAnswerEnum = (AnswerEnum)anwer;
            if (rightAnswerEnum != 0)
                return rightAnswerEnum.ToString();
            else
                return "";
        }

    }
}
