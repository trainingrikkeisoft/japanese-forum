﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.ClassViewModels
{
    public class IndexClassViewModel
    {
        public IndexClassViewModel()
        {
            Classes = new List<ClassItemViewModel>();
        }

        public List<ClassItemViewModel> Classes { get; set; }
    }
}
