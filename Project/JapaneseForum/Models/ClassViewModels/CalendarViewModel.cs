﻿using JapaneseForum.Const;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.ClassViewModels
{
    public class CalendarViewModel : IValidatableObject
    {
        public CalendarViewModel()
        {
            Dow = new Dictionary<int, bool>();
            Dow[ConstWeek.Monday] = false;
            Dow[ConstWeek.Tuesday] = false;
            Dow[ConstWeek.Wednesday] = false;
            Dow[ConstWeek.Thursday] = false;
            Dow[ConstWeek.Friday] = false;
            Dow[ConstWeek.Saturday] = false;
            Dow[ConstWeek.Sunday] = false;
        }

        public string Id { get; set; }

        public string ClassId { get; set; }

        [Display(Name = "Tiêu đề")]
        [Required(ErrorMessage = "Hãy nhập tiêu đề")]
        public string Title { get; set; }

        [Display(Name = "Phòng học")]
        public string Location { get; set; }

        [Display(Name = "Ngày bắt đầu")]
        [Required(ErrorMessage = "Hãy nhập ngày bắt đầu")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateStart { get; set; }
        
        [Display(Name = "Ngày kết thúc")]
        [Required(ErrorMessage = "Hãy nhập ngày kết thúc")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyy}", ApplyFormatInEditMode = true)]
        public DateTime DateEnd { get; set; }

        [Display(Name = "Thời gian bắt đầu")]
        [DataType(DataType.Time)]
        public TimeSpan? TimeStart { get; set; }

        [Display(Name = "Thời gian kết thúc")]
        [DataType(DataType.Time)]
        public TimeSpan? TimeEnd { get; set; }

        [Display(Name = "Cả ngày")]
        public bool AllDay { get; set; }
        
        [Display(Name = "Lặp")]
        public int RepeatType { get; set; } = 0;

        public Dictionary<int, bool> Dow { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            var pDateStart = new[] { "DateStart" };
            var pDateEnd = new[] { "DateEnd" };
            var pTimeStart = new[] { "TimeStart" };
            var pTimeEnd = new[] { "TimeEnd" };
            if (!AllDay && TimeStart == null)
                results.Add(new ValidationResult("Hãy nhập giờ bắt đầu", pTimeStart));
            if (!AllDay && TimeEnd == null)
                results.Add(new ValidationResult("Hãy nhập giờ kết thúc", pTimeEnd));

            if (DateTime.Compare(DateStart, DateEnd) > 0)
                results.Add(new ValidationResult("Ngày kết thúc phải sau ngày bắt đầu", pDateEnd));
            
            if((RepeatType != ConstRepeatType.NotRepeat || DateTime.Compare(DateStart, DateEnd) == 0)
                && TimeStart != null && TimeEnd != null
                && TimeSpan.Compare(TimeStart ?? TimeSpan.MinValue, TimeEnd ?? TimeSpan.MinValue) >= 0)
                    results.Add(new ValidationResult("Giờ kết thúc phải sau giờ bắt đầu", pTimeEnd));
            return results;
        }
    }
}
