﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.ClassViewModels
{
    public class ClassItemViewModel : BaseEntityOwner
    {
        public ClassItemViewModel(Class item)
        {
            EnterData(item);
        }
        
        public string Id { get; set; }
        
        [Display(Name ="Tên lớp")]
        public string Name { get; set; }

        public virtual ICollection<ClassStudent> ClassStudents { get; set; }

        public virtual ICollection<TimeTable> TimeTables { get; set; }

        public virtual ICollection<Labor> Labors { get; set; }

        [Display(Name = "Giáo viên chủ nhiệm")]
        public string TeacherId { get; set; }

        [Display(Name = "Giáo viên chủ nhiệm")]
        public virtual ApplicationUser Teacher { get; set; }

        public void EnterData(Class item)
        {
            Id = item.Id;
            Name = item.Name;
            ClassStudents = item.ClassStudents;
            TimeTables = item.TimeTables;
            Labors = item.Labors;
            CreatedBy = item.CreatedBy;
            UpdatedBy = item.UpdatedBy;
            CreatedAt = item.CreatedAt;
            UpdatedAt = item.UpdatedAt;
            CreatedOwner = item.CreatedOwner;
            UpdatedOwner = item.UpdatedOwner;
            TeacherId = item.TeacherId;
            Teacher = item.Teacher;
        }

    }
}
