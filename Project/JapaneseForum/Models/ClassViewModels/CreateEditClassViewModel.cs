﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.ClassViewModels
{
    public class CreateEditClassViewModel
    {
        public CreateEditClassViewModel()
        {

        }

        public CreateEditClassViewModel(Class classItem)
        {
            Name = classItem.Name;
            TeacherId = classItem.TeacherId;
        }

        public string Id { get; set; }

        [Required(ErrorMessage = "Nhập tên lớp")]
        [Display(Name = "Tên lớp")]
        public string Name { get; set; }

        [Display(Name = "Giáo viên chủ nhiệm")]
        [Required(ErrorMessage ="chọn giáo viên chủ nhiệm")]
        public string TeacherId { get; set; }

        public SelectList TeacherList { get; set; }
    }
}
