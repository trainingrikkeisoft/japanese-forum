﻿using JapaneseForum.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.ClassViewModels
{
    public class StudentClassViewModel
    {
        public StudentClassViewModel()
        {
            Students = new List<UserItemViewModel>();
        }

        public Class Class { get; set; }

        public List<UserItemViewModel> Students { get; set; }
    }
}
