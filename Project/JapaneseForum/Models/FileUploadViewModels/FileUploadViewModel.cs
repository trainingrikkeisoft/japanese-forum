﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.FileUploadViewModels
{
    public class FileUploadViewModel
    {
        public IFormFile ImageName { get; set; }
    }
}
