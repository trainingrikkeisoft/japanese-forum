﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models
{
    public class BaseEntityOwner
    {
        [Display(Name = "Người tạo")]
        public string CreatedBy { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime? CreatedAt { get; set; }

        [Display(Name = "Người cập nhật")]
        public string UpdatedBy { get; set; }

        [Display(Name = "Ngày cập nhật")]
        public DateTime? UpdatedAt { get; set; }

        public virtual ApplicationUser CreatedOwner { get; set; }

        public virtual ApplicationUser UpdatedOwner { get; set; }
    }
}
