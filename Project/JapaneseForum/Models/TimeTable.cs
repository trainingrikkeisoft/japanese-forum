﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models
{
    public class TimeTable : BaseEntityOwner
    {
        public TimeTable()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string ClassId { get; set; }
        
        [Required]
        public TimeSpan TimeStart { get; set; }

        [Required]
        public TimeSpan TimeEnd { get; set; }

        public bool IsAllDay { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        /// <summary>
        /// 0 : not repeat
        /// 1 : daily
        /// 2 : weekly
        /// 3 : yearly
        /// </summary>
        public int RepeatType { get; set; }

        /// <summary>
        /// ex : RepeatNo = "1, 3"
        /// if repeatType is weekly repeat date is Monday and Thurday
        /// if repeattype is monthly repeat date is start date
        /// if repeatType is yearly repeat date is start date
        /// </summary>
        public string  RepeatNo { get; set; }

        public string Location { get; set; }
        
        public virtual Class Class { get; set; }
    }
}
