﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models
{
    [Table("TestAnswers")]
    public partial class TestAnswer
    {
        [Key, Column(Order = 1)]
        public string UserId { get; set; }

        [Key, Column(Order = 2)]
        public int QuestionId { get; set; }

        public int Answer { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual Question Question { get; set; }
    }
}
