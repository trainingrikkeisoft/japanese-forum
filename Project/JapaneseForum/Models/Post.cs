﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("Posts")]
    public partial class Post : BaseEntityOwner
    {
        public Post()
        {
            Comments = new HashSet<Comment>();
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Thumbnail { get; set; }

        [Column(TypeName ="NTEXT")]
        public string Content { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public int Status { get; set; } = 1;

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual Category Category { get; set; }

        public int TimeRead { get; set; }
    }
}
