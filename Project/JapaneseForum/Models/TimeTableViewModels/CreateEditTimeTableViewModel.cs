﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.TimeTableViewModels
{
    public class CreateEditTimeTableViewModel
    {
        public string Id { get; set; }
        public int ClassId { get; set; }
        public DateTime Day { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
    }
}
