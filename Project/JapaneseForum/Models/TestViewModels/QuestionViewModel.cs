﻿using JapaneseForum.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.TestViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }

        public int No { get; set; }

        public int QuestionId { get; set; }

        public string Content { get; set; }

        public string AnswerA { get; set; }

        public string AnswerB { get; set; }

        public string AnswerC { get; set; }

        public string AnswerD { get; set; }

        [Required(ErrorMessage = "Hãy trả lời câu hỏi")]
        public int? ChoosenAnswer { get; set; }

        public int RightAnswer { get; set; }
        public string RightAnswerText
        {
            get
            {
                return ConvertToText.ConvertAnswerToText(RightAnswer);
            }
        }
    }
}
