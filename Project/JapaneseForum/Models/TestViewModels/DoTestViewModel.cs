﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.TestViewModels
{
    public class DoTestViewModel
    {
        public DoTestViewModel()
        {
            QuestionTypeList = new List<QuestionTypeViewModel>();
        }
        public int TestId { get; set; }

        public string TestName { get; set; }

        public int TestTime { get; set; }

        public int Mark { get; set; }

        public List<QuestionTypeViewModel> QuestionTypeList { get; set; }
    }
}
