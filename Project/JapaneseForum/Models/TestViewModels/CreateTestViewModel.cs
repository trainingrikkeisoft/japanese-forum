﻿using JapaneseForum.Const;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.TestViewModels
{
    public class CreateTestViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Hãy nhập tên bài kiểm tra")]
        [Display(Name = "Tên bài kiểm tra")]
        public string TestName { get; set; }

        [Range(0, 120000, ErrorMessage = "Định dạng thời gian không đúng")]
        [Display(Name = "Thời gian làm bài (phút)")]
        public int TestTime { get; set; }

        [Display(Name = "Trạng thái")]
        public StatusEnum Status { get; set; }

        public SelectList StatusList;
    }
}
