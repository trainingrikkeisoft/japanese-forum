﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.TestViewModels
{
    public class ListTestViewModel
    {
        public ListTestViewModel()
        {
            ListTest = new List<TestViewModel>();
        }
        public List<TestViewModel> ListTest { get; set; }
    }
}
