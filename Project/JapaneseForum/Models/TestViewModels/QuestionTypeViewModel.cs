﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.TestViewModels
{
    public class QuestionTypeViewModel
    {
        public QuestionTypeViewModel()
        {
            this.QuestionList = new List<QuestionViewModel>();
        }

        public string TypeName { get; set; }

        public List<QuestionViewModel> QuestionList { get; set; }
    }
}
