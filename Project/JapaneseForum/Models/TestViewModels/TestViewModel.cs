﻿using JapaneseForum.Classes;
using JapaneseForum.Helper;
using JapaneseForum.Const;
using System;
using System.Linq;

namespace JapaneseForum.Models.TestViewModels
{
    public class TestViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusEnum Status { get; set; }

        public string StatusText
        {
            get
            {
                return ConvertToText.ConvertStatusToText(Status);
            }
        }

        public string CreatedOwner { get; set; }

        public DateTime? CreatedAt { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string UpdatedBy { get; set; }

        public string UpdatedOwner { get; set; }

        private StatusCollection statusList;

        public TestViewModel(Test test)
        {
            statusList = new StatusCollection();
            Id = test.Id;
            Name = test.Name;
            var status = statusList.Collection.Where(i => (int)(i.Id) == test.Status);
            Status = status.Count() == 0 ? 0 : status.First().Id;
            CreatedBy = test.CreatedBy;
            CreatedOwner = test.CreatedOwner?.UserName;
            CreatedAt = test.CreatedAt;
            UpdatedAt = test.UpdatedAt;
            UpdatedBy = test.UpdatedBy;
            UpdatedOwner = test.UpdatedOwner?.UserName;
        }
    }
}
