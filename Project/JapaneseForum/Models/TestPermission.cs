﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models
{
    [Table("TestPermissions")]
    public class TestPermission
    {
        [Key]
        public int TestId { get; set; }

        [Key]
        public string UserId { get; set; }

        public bool IsAllowed { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual Test Test { get; set; }
    }
}
