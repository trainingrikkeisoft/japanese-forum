﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.HomeViewModels
{
    public class HomeIndexViewModel
    {
        public HomeIndexViewModel()
        {
            ListCategoryWithPosts = new List<CategoryWithPostsViewModel>();
        }

        public List<CategoryWithPostsViewModel> ListCategoryWithPosts { get; set; }

        public CategoryWithPostsViewModel MostReadPosts { get; set; }

        public CategoryWithPostsViewModel CarouselPosts { get; set; }
    }
}
