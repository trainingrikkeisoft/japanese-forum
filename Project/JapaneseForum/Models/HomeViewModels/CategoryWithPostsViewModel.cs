﻿using JapaneseForum.Models.PostViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.HomeViewModels
{
    public class CategoryWithPostsViewModel
    {
        public string CategoryName { get; set; }

        public string CategoryLink { get; set; }

        public List<PostViewModel> ListTopPosts { get; set; }
    }
}
