﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.CommentViewModels
{
    public class CommentItemViewModel : BaseEntityOwner
    {
        public CommentItemViewModel(Comment comment)
        {
            Id = comment.Id;
            Content = comment.Content;
            PostId = comment.PostId;
        }

        public int Id { get; set; }

        public string Content { get; set; }

        public int PostId { get; set; }

        public virtual Post Post { get; set; }
    }
}
