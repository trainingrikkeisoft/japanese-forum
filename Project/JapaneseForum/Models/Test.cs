﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("Tests")]
    public partial class Test : BaseEntityOwner
    {
        public Test()
        {
            Questions = new HashSet<Question>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int TestTime { get; set; }

        public int Status { get; set; } = 1;

        public virtual ICollection<Question> Questions { get; set; }

        public virtual ICollection<TestPermission> TestPermission { get; set; }
    }
}
