﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("Labors")]
    public partial class Labor : BaseEntityOwner
    {
        [Key]
        public int Id { get; set; }

        public string ClassId { get; set; }
        
        public string WorkerId { get; set; }

        public DateTime Day { get; set; }

        public virtual ApplicationUser Worker { get; set; }

        public virtual Class Class { get; set; }
    }
}
