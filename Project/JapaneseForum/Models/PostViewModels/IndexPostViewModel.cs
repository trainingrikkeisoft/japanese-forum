﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.PostViewModels
{
    public class IndexPostViewModel
    {
        public IndexPostViewModel()
        {
            ListPost = new List<PostViewModel>();
        }

        public List<Post> FullListPost { get; set; }

        public List<PostViewModel> ListPost{ get; set; }

        public int PageSize { get; set; }

        public int Page => (int)Math.Ceiling((double)FullListPost.Count() / PageSize);

        public int CurrentPage { get; set; }

    }
}
