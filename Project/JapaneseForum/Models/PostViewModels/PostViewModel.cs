﻿using JapaneseForum.Helper;
using JapaneseForum.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace JapaneseForum.Models.PostViewModels
{
    public class PostViewModel
    {
        public PostViewModel(Post post)
        {
            Id = post.Id;
            Title = post.Title;
            Thumbnail = post.Thumbnail;
            Content = post.Content;
            CategoryId = post.CategoryId;
            Category = post.Category;
            CreatedAt = post.CreatedAt ?? DateTime.MinValue;
            UpdatedAt = post.UpdatedAt ?? DateTime.MinValue;
            CreatedBy = post.CreatedBy;
            CreatedOwner = post.CreatedOwner;
            UpdatedOwner = post.UpdatedOwner;
            Status = post.Status;
            Comments = post.Comments.OrderByDescending(i => i.CreatedBy).ToList();
        }

        public int Id { get; set; }

        [Display(Name = "Tiêu đề")]
        public string Title { get; set; }

        private string _thumbnail;

        [Display(Name = "Ảnh")]
        public string Thumbnail
        {
            get
            {
                var startIndex = Content.IndexOf("src=") + 5;
                if (startIndex > -1)
                {
                    var endIndex = startIndex + Content.Substring(startIndex).IndexOf("\"");
                    _thumbnail = startIndex < endIndex ? Content.Substring(startIndex, endIndex - startIndex) : ConstConfiguration.DefaultThumbnail;
                }
                else
                {
                    _thumbnail = ConstConfiguration.DefaultThumbnail;
                }
                return _thumbnail;
            }
            set
            {
                _thumbnail = value;
            }
        }

        [Display(Name = "Tóm tắt")]
        public string Summary
        {
            get
            {
                int start = 0, end = 0;
                string newContent = Content, summary = string.Empty;
                while ((start = Content.IndexOf("<img", start)) != -1)
                {
                    end = start + Content.Substring(start).IndexOf(" />");
                    if (end > -1 && end < newContent.Length)
                        newContent = newContent.Remove(start, end - start + 3);
                    start++;
                }
                List<string> replaceStrList = new List<string>(new string[] { "<h1>", "<h2>", "<h3>", "<h4>", "</h1>", "</h2>", "</h3>", "</h4>" });
                replaceStrList.ForEach(i => summary = newContent.Replace(i, ""));
                summary = summary.Substring(0, summary.Length > 200 ? 200 : summary.Length) + " ...";

                return summary;
            }
        }

        [Display(Name = "Nội dung")]
        public string Content { get; set; }

        [Display(Name = "Đề tài")]
        public int CategoryId { get; set; }

        [Display(Name = "Đề tài")]
        public Category Category { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreatedAt { get; set; }

        [Display(Name = "Ngày update")]
        public DateTime UpdatedAt { get; set; }

        [Display(Name = "Người tạo")]
        public string CreatedBy { get; set; }

        [Display(Name = "Người update")]
        public string UpdatedBy { get; set; }

        [Display(Name = "Người tạo")]
        public ApplicationUser CreatedOwner { get; set; }

        [Display(Name = "Người update")]
        public ApplicationUser UpdatedOwner { get; set; }

        [Display(Name = "Trạng thái")]
        public int Status { get; set; }

        [Display(Name = "Trạng thái")]
        public string StatusText
        {
            get
            {
                return ConvertToText.ConvertStatusToText(Status);
            }
        }

        [Display(Name = "Bình luận")]
        [Required(ErrorMessage = "Nhập bình luận")]
        public string NewComment { get; set; }

        public List<Comment> Comments { get; set; }
    }
}
