﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using JapaneseForum.Const;

namespace JapaneseForum.Models.PostViewModels
{
    public class CreatePostViewModel : BaseEntityOwner
    {
        public CreatePostViewModel()
        {
        }
        public CreatePostViewModel(Post post)
        {
            Title = post.Title;
            Content = post.Content;
            CategoryId = post.CategoryId;
            Category = post.Category;
            Status = (StatusEnum)post.Status;
            CreatedAt = post.CreatedAt;
            CreatedBy = post.CreatedBy;
            CreatedOwner = post.CreatedOwner;
            UpdatedAt = post.UpdatedAt;
            UpdatedBy = post.UpdatedBy;
            UpdatedOwner = post.UpdatedOwner;
        }

        [Required(ErrorMessage ="Hãy nhập tiêu đề bài viết")]
        public string Title { get; set; }

        [Display(Name = "Nội dung")]
        [Required(ErrorMessage = "Hãy nhập nội dung bài viết")]
        public string Content { get; set; }

        [Required(ErrorMessage = "Hãy chọn chủ đề")]
        public int CategoryId { get; set; }
        
        public Category Category { get; set; }

        [Display(Name = "Trạng thái")]
        [Required(ErrorMessage = "Hãy chọn trạng thái bài viết")]
        public StatusEnum Status { get; set; }

        public SelectList StatusList;

        public SelectList CategoryList;

    }
}
