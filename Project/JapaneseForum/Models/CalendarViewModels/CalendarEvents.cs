﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JapaneseForum.Models.CalendarViewModels
{
    public class CalendarEvents
    {
        private List<CalendarEvent> CalendarEventList { get; set; }

        public CalendarEvents()
        {
            CalendarEventList = new List<CalendarEvent>();
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(CalendarEventList);
        }

        public List<CalendarEvent> Deserialize(string jsonStr)
        {
            return (List<CalendarEvent>)JsonConvert.DeserializeObject(jsonStr);
        }

        public void Add(CalendarEvent e)
        {
            CalendarEventList.Add(e);
        }
    }
}
