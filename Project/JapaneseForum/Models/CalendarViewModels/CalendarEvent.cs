﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.CalendarViewModels
{
    public class CalendarEvent
    {

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        /// <summary>
        /// start of event format date if repeat format time
        /// </summary>
        [JsonProperty(PropertyName = "start")]
        public string Start { get; set; }

        /// <summary>
        /// end of event format date if repeat format time
        /// </summary>
        [JsonProperty(PropertyName = "end")]
        public string End { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "allDay")]
        public bool AllDay { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "url")]
        public string Url { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "className")]
        public string ClassName { get; set; }

        /// <summary>
        /// 1. Daily
        /// 2. Weekly
        /// 3. Monthly
        /// 4. Yearly
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "repeat")]
        public int Repeat { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "dow")]
        public string[] Dow { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "ranges")]
        public List<CalendarRange> Ranges { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "backgroundColor")]
        public string BackgroundColor { get; set; }
    }
}
