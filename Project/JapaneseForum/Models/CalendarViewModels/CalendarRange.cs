﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.CalendarViewModels
{
    public class CalendarRange
    {

        public CalendarRange()
        {

        }

        public CalendarRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        [JsonProperty(PropertyName = "start")]
        public DateTime Start { get; set; }

        [JsonProperty(PropertyName = "end")]
        public DateTime End { get; set; }
    }
}
