﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("Media")]
    public partial class Media
    {
        public Media()
        {
            Questions = new HashSet<Question>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "VARCHAR(100)")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Audio = 1,  Video = 2, Image = 3
        /// </summary>
        [Required]
        public int Type { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}
