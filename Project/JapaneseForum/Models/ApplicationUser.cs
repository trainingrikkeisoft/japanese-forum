﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace JapaneseForum.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            ClassStudents = new HashSet<ClassStudent>();
            CreatedClasses = new HashSet<Class>();
            UpdatedClasses = new HashSet<Class>();
            CreatedTimeTables = new HashSet<TimeTable>();
            UpdatedTimeTables = new HashSet<TimeTable>();
            Labors = new HashSet<Labor>();
            CreatedLabors = new HashSet<Labor>();
            UpdatedLabors = new HashSet<Labor>();
            CreatedComments = new HashSet<Comment>();
            UpdatedComments = new HashSet<Comment>();
            CreatedPosts = new HashSet<Post>();
            UpdatedPosts = new HashSet<Post>();
            CreatedTests = new HashSet<Test>();
            UpdatedTests = new HashSet<Test>();
            TestPermisions = new HashSet<TestPermission>();
        }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual int Gender { get; set; }

        public virtual DateTime Birthday { get; set; }

        public virtual ICollection<ClassStudent> ClassStudents { get; set; }

        //Class
        public virtual ICollection<Class> CreatedClasses { get; set; }

        public virtual ICollection<Class> UpdatedClasses { get; set; }

        public virtual ICollection<Class> IncumbentClasses { get; set; }

        //Time Table
        public virtual ICollection<TimeTable> CreatedTimeTables { get; set; }

        public virtual ICollection<TimeTable> UpdatedTimeTables { get; set; }

        //Labor
        public virtual ICollection<Labor> Labors { get; set; }

        public virtual ICollection<Labor> CreatedLabors { get; set; }

        public virtual ICollection<Labor> UpdatedLabors { get; set; }

        //Comment
        public virtual ICollection<Comment> CreatedComments { get; set; }

        public virtual ICollection<Comment> UpdatedComments { get; set; }

        //Post
        public virtual ICollection<Post> CreatedPosts { get; set; }

        public virtual ICollection<Post> UpdatedPosts { get; set; }

        //Test
        public virtual ICollection<Test> CreatedTests { get; set; }

        public virtual ICollection<Test> UpdatedTests { get; set; }
        
        //Test Permission
        public virtual ICollection<TestPermission> TestPermisions { get; set; }

        //Fine

        public virtual ICollection<Fine> CreatedFines { get; set; }

        public virtual ICollection<Fine> UpdatedFines { get; set; }

        public virtual ICollection<Fine> Fines { get; set; }
    }
}
