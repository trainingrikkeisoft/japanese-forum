﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models
{
    [Table("Fines")]
    public class Fine : BaseEntityOwner
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ClassId { get; set; }

        public string StudentId { get; set; }

        public int AmountOfMoney { get; set; }

        public bool IsPay { get; set; }

        public string Reason { get; set; }

        public virtual Class Class { get; set; }

        public virtual ApplicationUser Student { get; set; }
    }
}
