﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.FineViewModels
{
    public class IndexFineViewModel
    {
        public Class Class { get; set; }

        public List<FineItemViewModel> FineList { get; set; }

        [Display(Name = "Tổng")]
        public int Sum {
            get {
                return FineList.Select(i => i.AmountOfMoney).Sum();
            }
        }
    }
}
