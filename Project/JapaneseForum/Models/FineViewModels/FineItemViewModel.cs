﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.FineViewModels
{
    public class FineItemViewModel : BaseEntityOwner
    {
        public FineItemViewModel()
        {

        }

        public FineItemViewModel(Fine fine)
        {
            Id = fine.Id;
            ClassId = fine.ClassId;
            StudentId = fine.StudentId;
            AmountOfMoney = fine.AmountOfMoney;
            IsPay = fine.IsPay;
            Reason = fine.Reason;
            Class = fine.Class;
            Student = fine.Student;
            CreatedAt = fine.CreatedAt;
            CreatedBy = fine.CreatedBy;
            CreatedOwner = fine.CreatedOwner;
            UpdatedAt = fine.UpdatedAt;
            UpdatedBy = fine.UpdatedBy;
            UpdatedOwner = fine.UpdatedOwner;
        }

        public int Id { get; set; }

        public string ClassId { get; set; }

        public string StudentId { get; set; }

        [Display(Name = "Số tiền")]
        public int AmountOfMoney { get; set; }

        [Display(Name = "Đã nộp phạt")]
        public bool IsPay { get; set; }

        [Display(Name = "Lý do phạt")]
        public string Reason { get; set; }

        [Display(Name = "Lớp học")]
        public virtual Class Class { get; set; }

        [Display(Name = "Học viên")]
        public virtual ApplicationUser Student { get; set; }

        public SelectList StudentList { get; set; }
    }
}
