﻿using JapaneseForum.Const;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.UserViewModels
{
    public class AddOrEditUserViewModel
    {
        public AddOrEditUserViewModel()
        {
            RoleList = new Dictionary<string, bool>();
        }

        public string Id { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Trường email không được để trống")]
        [EmailAddress(ErrorMessage = "Trường Email nhập vào không hợp lệ")]
        [RegularExpression(@"^(([^<>()[\]\\.,;:\s@""]+(\.[^<>()[\]\\.,;:\s@""]+)*)|("".+""))@(rikkeisoft.com)$", ErrorMessage ="Vui lòng nhập mail rikkeisoft")]
        public string Email { get; set; }

        [Display(Name = "Họ")]
        [Required(ErrorMessage = "Trường Họ không được để trống")]
        public string FirstName { get; set; }

        [Display(Name = "Tên")]
        [Required(ErrorMessage = "Trường Tên không được để trống")]
        public string LastName { get; set; }

        [Display(Name = "Quyền")]
        [Required(ErrorMessage = "Trường quyền không được để trống")]
        public List<string> SelectedRoles { get; set; }

        [Display(Name = "Giới tính")]
        public int Gender { get; set; }

        [Display(Name = "Ngày sinh")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthday { get; set; } = new DateTime(1990, 1, 1);

        [Display(Name = "Điện thoại")]
        [Phone]
        public string PhoneNumber { get; set; }

        public Dictionary<string, bool> RoleList;

    }
}
