﻿using JapaneseForum.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.User
{
    public class UserItemViewModel : IdentityUser
    {
        public UserItemViewModel(ApplicationUser item)
        {
            Id = item.Id;
            FirstName = item.FirstName;
            LastName = item.LastName;
            Gender = item.Gender;
            Birthday = item.Birthday;
            UserName = item.UserName;
            Email = item.Email;
            PhoneNumber = item.PhoneNumber;
        }

        [Display(Name ="Tên")]
        public virtual string FirstName { get; set; }

        [Display(Name = "Họ")]
        public virtual string LastName { get; set; }

        [Display(Name = "Giới tính")]
        public virtual int Gender { get; set; }

        [Display(Name = "Ngày sinh")]
        public virtual DateTime Birthday { get; set; }
    }
}
