﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("Questions")]
    public partial class Question
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int TestId { get; set; }

        [Required]
        public int TypeId { get; set; }

        [Required]
        [Column(TypeName ="NTEXT")]
        public string Content { get; set; }

        [Required]
        [Column(TypeName = "NTEXT")]
        public string AnswerA { get; set; }

        [Required]
        [Column(TypeName = "NTEXT")]
        public string AnswerB { get; set; }

        [Required]
        [Column(TypeName = "NTEXT")]
        public string AnswerC { get; set; }

        [Required]
        [Column(TypeName = "NTEXT")]
        public string AnswerD { get; set; }

        [Required]
        public int RightAnswer { get; set; }

        public int? MediaId { get; set; }

        public virtual Media Media { get; set; }
        public virtual Test Test { get; set; }
        public virtual QuestionType Type { get; set; }
    }
}
