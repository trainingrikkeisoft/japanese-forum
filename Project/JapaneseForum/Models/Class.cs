﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("Classes")]
    public partial class Class : BaseEntityOwner
    {
        public Class()
        {
            ClassStudents = new HashSet<ClassStudent>();
            TimeTables = new HashSet<TimeTable>();
            Labors = new HashSet<Labor>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string TeacherId { get; set; }

        public virtual ApplicationUser Teacher { get; set; }

        public virtual ICollection<ClassStudent> ClassStudents { get; set; }

        public virtual ICollection<TimeTable> TimeTables { get; set; }

        public virtual ICollection<Labor> Labors { get; set; }

        public virtual ICollection<Fine> Fines { get; set; }
    }
}
