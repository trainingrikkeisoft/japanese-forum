﻿using JapaneseForum.Models.CalendarViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.LaborViewModels
{
    public class LaborIndexViewModel
    {
        public CalendarEvents CalendarEvents { get; set; }

        public string CalendarEventsJsonStr
        {
            get
            {
                return CalendarEvents.Serialize();
            }
        }

        public SelectList ClassList { get; set; }
    }
}
