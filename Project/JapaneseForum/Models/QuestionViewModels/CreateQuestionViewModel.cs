﻿using JapaneseForum.Const;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Models.QuestionViewModels
{
    public class CreateQuestionViewModel
    {
        public int No { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage ="Hãy nhập nội dung")]
        [Display(Name = "Nội dung")]
        public string Content { get; set; }

        [Required(ErrorMessage = "Hãy nhập đáp án A")]
        [Display(Name = "Đáp án A")]
        public string AnswerA { get; set; }

        [Required(ErrorMessage = "Hãy nhập đáp án B")]
        [Display(Name = "Đáp án B")]
        public string AnswerB { get; set; }

        [Required(ErrorMessage = "Hãy nhập đáp án C")]
        [Display(Name = "Đáp án C")]
        public string AnswerC { get; set; }

        [Required(ErrorMessage = "Hãy nhập đáp án D")]
        [Display(Name = "Đáp án  D")]
        public string AnswerD { get; set; }

        [Required(ErrorMessage = "Hãy chọn đáp án đúng")]
        [Display(Name = "Câu trả lời đúng")]
        public AnswerEnum RightAnswer { get; set; }

        [Required(ErrorMessage = "Hãy chọn loại câu hỏi")]
        [Display(Name = "Loại câu hỏi")]
        public int Type { get; set; }

        public SelectList QuestionTypeList { get; set; }

        public SelectList AnwerList { get; set; }
        
        public int TestID { get; set; }
    }
}
