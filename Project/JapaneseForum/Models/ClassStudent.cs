﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JapaneseForum.Models
{
    [Table("ClassStudent")]
    public partial class ClassStudent
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ClassId { get; set; }

        public virtual Class Class { get; set; }

        public virtual ApplicationUser Student { get; set; }
    }
}
