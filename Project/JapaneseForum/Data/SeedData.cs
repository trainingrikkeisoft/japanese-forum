﻿using JapaneseForum.Const;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace JapaneseForum.Data
{
    public static class SeedData
    {
        public static async Task InitializeAsync(IServiceProvider serviceProvider, string adminMail)
        {
            var db = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            AddClass(db);
            AddCategory(db);
            AddQuestionType(db);
            await AddRoleAsync(roleManager);
            await CreateUserAdminAsync(userManager, adminMail);
        }

        private static void AddClass(ApplicationDbContext db)
        {
            if (db.Classes.Any())
            {
                return;
            }

            db.AddRange(
                new Class { Name = "N1" },
                new Class { Name = "N2" }
                );

            db.SaveChanges();
        }

        private static void AddCategory(ApplicationDbContext db)
        {
            var allCategory = GetFieldValues(new ConstPostCategory());
            foreach (var item in allCategory)
            {
                if (db.Categories.Where(i => i.Name.Equals(item.Value)).Count() <= 0)
                    db.Add(new Category() { Name = item.Value });
            }

            db.SaveChanges();
        }

        private static void AddQuestionType(ApplicationDbContext db)
        {
            if (db.QuestionTypes.Any())
                return;

            db.QuestionTypes.AddRange(
                new QuestionType { Name = "Từ vựng" },
                new QuestionType { Name = "Ngữ pháp" },
                new QuestionType { Name = "Kanzi" },
                new QuestionType { Name = "Đọc hiểu" },
                new QuestionType { Name = "Nghe" }
                );

            db.SaveChanges();
        }

        private static async Task AddRoleAsync(RoleManager<IdentityRole> roleManager)
        {
            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_ADMIN))
            {
                await roleManager.CreateAsync(new IdentityRole(ConstAuthozation.ROLE_ADMIN));
            }

            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_PRESIDENT))
            {
                await roleManager.CreateAsync(new IdentityRole(ConstAuthozation.ROLE_PRESIDENT));
            }

            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_TEACHER))
            {
                await roleManager.CreateAsync(new IdentityRole(ConstAuthozation.ROLE_TEACHER));
            }

            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_STUDENT))
            {
                await roleManager.CreateAsync(new IdentityRole(ConstAuthozation.ROLE_STUDENT));
            }
        }

        private static async Task CreateUserAdminAsync(UserManager<ApplicationUser> userManager, string adminMail)
        {
            var user = await userManager.FindByNameAsync(adminMail);
            if (user == null)
            {
                user = new ApplicationUser { UserName = adminMail, Email = adminMail };
                await userManager.CreateAsync(user);
            }
            await userManager.AddToRoleAsync(user, ConstAuthozation.ROLE_ADMIN);
        }

        private static Dictionary<string, string> GetFieldValues(object obj)
        {
            return obj.GetType()
                      .GetProperties()
                      .Where(f => f.PropertyType == typeof(string))
                      .ToDictionary(f => f.Name,
                                    f => (string)f.GetValue(null));
        }
    }
}
