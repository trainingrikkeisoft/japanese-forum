﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using JapaneseForum.Models;

namespace JapaneseForum.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole, string>
    {
        public DbSet<Class> Classes { get; set; }

        public DbSet<Labor> Labors { get; set; }

        public DbSet<ClassStudent> ClassStudent { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Test> Tests { get; set; }

        public DbSet<QuestionType> QuestionTypes { get; set; }

        public DbSet<Media> Media { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<TestAnswer> TestAnswer { get; set; }

        public DbSet<TimeTable> TimeTables { get; set; }

        public DbSet<TestPermission> TestPermissions { get; set; }

        public DbSet<Fine> Fines { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            //Comment
            builder.Entity<Comment>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedComments)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<Comment>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedComments)
                .HasForeignKey(s => s.UpdatedBy);

            //Post
            builder.Entity<Post>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedPosts)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<Post>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedPosts)
                .HasForeignKey(s => s.UpdatedBy);

            //Test
            builder.Entity<Test>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedTests)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<Test>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedTests)
                .HasForeignKey(s => s.UpdatedBy);

            //TestAnswer
            builder.Entity<TestAnswer>()
                .HasKey(s => new { s.UserId, s.QuestionId });

            //Class
            builder.Entity<Class>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedClasses)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<Class>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedClasses)
                .HasForeignKey(s => s.UpdatedBy);

            builder.Entity<Class>()
                .HasOne(s => s.Teacher)
                .WithMany(s => s.IncumbentClasses)
                .HasForeignKey(s => s.TeacherId);

            //ClassStudent
            builder.Entity<ClassStudent>()
                .HasKey(bc => new { bc.ClassId, bc.UserId });

            builder.Entity<ClassStudent>()
                .HasOne(bc => bc.Student)
                .WithMany(b => b.ClassStudents)
                .HasForeignKey(bc => bc.UserId);

            builder.Entity<ClassStudent>()
                .HasOne(bc => bc.Class)
                .WithMany(c => c.ClassStudents)
                .HasForeignKey(bc => bc.ClassId);

            //Time table foregin key
            builder.Entity<TimeTable>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedTimeTables)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<TimeTable>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedTimeTables)
                .HasForeignKey(s => s.UpdatedBy);

            builder.Entity<TimeTable>()
                .HasOne(s => s.Class)
                .WithMany(s => s.TimeTables)
                .HasForeignKey(s => s.ClassId);

            //Labor
            builder.Entity<Labor>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedLabors)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<Labor>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedLabors)
                .HasForeignKey(s => s.UpdatedBy);

            builder.Entity<Labor>()
               .HasOne(s => s.Worker)
               .WithMany(s => s.Labors)
               .HasForeignKey(s => s.WorkerId);

            builder.Entity<Labor>()
               .HasOne(s => s.Class)
               .WithMany(s => s.Labors)
               .HasForeignKey(s => s.ClassId);

            // TestPermission
            builder.Entity<TestPermission>()
                .HasKey(s => new { s.UserId, s.TestId });

            builder.Entity<TestPermission>()
                .HasOne(s => s.Test)
                .WithMany(s => s.TestPermission)
                .HasForeignKey(s => s.TestId);

            builder.Entity<TestPermission>()
                .HasOne(s => s.User)
                .WithMany(s => s.TestPermisions)
                .HasForeignKey(s => s.UserId);

            //Fines
            builder.Entity<Fine>()
                .HasKey(s => new { s.Id});

            builder.Entity<Fine>()
                .HasOne(s => s.Class)
                .WithMany(s => s.Fines)
                .HasForeignKey(s => s.ClassId)
                .OnDelete(Microsoft.EntityFrameworkCore.Metadata.DeleteBehavior.SetNull);

            builder.Entity<Fine>()
                .HasOne(s => s.Student)
                .WithMany(s => s.Fines)
                .HasForeignKey(s => s.StudentId);

            builder.Entity<Fine>()
                .HasOne(s => s.CreatedOwner)
                .WithMany(s => s.CreatedFines)
                .HasForeignKey(s => s.CreatedBy);

            builder.Entity<Fine>()
                .HasOne(s => s.UpdatedOwner)
                .WithMany(s => s.UpdatedFines)
                .HasForeignKey(s => s.UpdatedBy);
        }
    }
}
