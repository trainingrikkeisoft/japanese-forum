﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace JapaneseForum.Data.Migrations
{
    public partial class UpdateTimeTableAndRelationshipBetweenTaimeTableAndLabor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Labors_TimeTables_TimeTableId",
                table: "Labors");

            migrationBuilder.DropForeignKey(
                name: "FK_Labors_AspNetUsers_WorkerId",
                table: "Labors");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_Labors_TimeTableId_WorkerId",
                table: "Labors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Labors",
                table: "Labors");

            migrationBuilder.DropColumn(
                name: "TimeTableId",
                table: "Labors");

            migrationBuilder.RenameColumn(
                name: "Day",
                table: "TimeTables",
                newName: "DateStart");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateEnd",
                table: "TimeTables",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsAllDay",
                table: "TimeTables",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "RepeatNo",
                table: "TimeTables",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RepeatType",
                table: "TimeTables",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "WorkerId",
                table: "Labors",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Labors",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<string>(
                name: "ClassId",
                table: "Labors",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Day",
                table: "Labors",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Labors",
                table: "Labors",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Labors_ClassId",
                table: "Labors",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_Labors_WorkerId",
                table: "Labors",
                column: "WorkerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Labors_Classes_ClassId",
                table: "Labors",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Labors_AspNetUsers_WorkerId",
                table: "Labors",
                column: "WorkerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Labors_Classes_ClassId",
                table: "Labors");

            migrationBuilder.DropForeignKey(
                name: "FK_Labors_AspNetUsers_WorkerId",
                table: "Labors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Labors",
                table: "Labors");

            migrationBuilder.DropIndex(
                name: "IX_Labors_ClassId",
                table: "Labors");

            migrationBuilder.DropIndex(
                name: "IX_Labors_WorkerId",
                table: "Labors");

            migrationBuilder.DropColumn(
                name: "DateEnd",
                table: "TimeTables");

            migrationBuilder.DropColumn(
                name: "IsAllDay",
                table: "TimeTables");

            migrationBuilder.DropColumn(
                name: "RepeatNo",
                table: "TimeTables");

            migrationBuilder.DropColumn(
                name: "RepeatType",
                table: "TimeTables");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Labors");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "Labors");

            migrationBuilder.DropColumn(
                name: "Day",
                table: "Labors");

            migrationBuilder.RenameColumn(
                name: "DateStart",
                table: "TimeTables",
                newName: "Day");

            migrationBuilder.AlterColumn<string>(
                name: "WorkerId",
                table: "Labors",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeTableId",
                table: "Labors",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Labors_TimeTableId_WorkerId",
                table: "Labors",
                columns: new[] { "TimeTableId", "WorkerId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Labors",
                table: "Labors",
                columns: new[] { "WorkerId", "TimeTableId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Labors_TimeTables_TimeTableId",
                table: "Labors",
                column: "TimeTableId",
                principalTable: "TimeTables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Labors_AspNetUsers_WorkerId",
                table: "Labors",
                column: "WorkerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
