﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JapaneseForum.Data.Migrations
{
    public partial class AddTableTestPermission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TestPermissions",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    TestId = table.Column<int>(nullable: false),
                    IsAllowed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestPermissions", x => new { x.UserId, x.TestId });
                    table.UniqueConstraint("AK_TestPermissions_TestId_UserId", x => new { x.TestId, x.UserId });
                    table.ForeignKey(
                        name: "FK_TestPermissions_Tests_TestId",
                        column: x => x.TestId,
                        principalTable: "Tests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestPermissions_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestPermissions");
        }
    }
}
