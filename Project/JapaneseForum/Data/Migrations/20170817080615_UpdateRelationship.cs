﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JapaneseForum.Data.Migrations
{
    public partial class UpdateRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Labors_AspNetUsers_UserId",
                table: "Labors");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_Labors_TimeTableId_UserId",
                table: "Labors");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Labors",
                newName: "WorkerId");

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Tests",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Posts",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Comments",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Labors_TimeTableId_WorkerId",
                table: "Labors",
                columns: new[] { "TimeTableId", "WorkerId" });

            migrationBuilder.CreateIndex(
                name: "IX_Tests_UpdatedBy",
                table: "Tests",
                column: "UpdatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_UpdatedBy",
                table: "Posts",
                column: "UpdatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UpdatedBy",
                table: "Comments",
                column: "UpdatedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_AspNetUsers_UpdatedBy",
                table: "Comments",
                column: "UpdatedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Labors_AspNetUsers_WorkerId",
                table: "Labors",
                column: "WorkerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_AspNetUsers_UpdatedBy",
                table: "Posts",
                column: "UpdatedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tests_AspNetUsers_UpdatedBy",
                table: "Tests",
                column: "UpdatedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_AspNetUsers_UpdatedBy",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Labors_AspNetUsers_WorkerId",
                table: "Labors");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_AspNetUsers_UpdatedBy",
                table: "Posts");

            migrationBuilder.DropForeignKey(
                name: "FK_Tests_AspNetUsers_UpdatedBy",
                table: "Tests");

            migrationBuilder.DropIndex(
                name: "IX_Tests_UpdatedBy",
                table: "Tests");

            migrationBuilder.DropIndex(
                name: "IX_Posts_UpdatedBy",
                table: "Posts");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_Labors_TimeTableId_WorkerId",
                table: "Labors");

            migrationBuilder.DropIndex(
                name: "IX_Comments_UpdatedBy",
                table: "Comments");

            migrationBuilder.RenameColumn(
                name: "WorkerId",
                table: "Labors",
                newName: "UserId");

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Tests",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Posts",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UpdatedBy",
                table: "Comments",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Labors_TimeTableId_UserId",
                table: "Labors",
                columns: new[] { "TimeTableId", "UserId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Labors_AspNetUsers_UserId",
                table: "Labors",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
