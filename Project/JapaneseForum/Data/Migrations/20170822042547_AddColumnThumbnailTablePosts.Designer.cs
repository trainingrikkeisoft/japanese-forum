﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using JapaneseForum.Data;

namespace JapaneseForum.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170822042547_AddColumnThumbnailTablePosts")]
    partial class AddColumnThumbnailTablePosts
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("JapaneseForum.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<DateTime>("Birthday");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<bool>("Gender");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("JapaneseForum.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("JapaneseForum.Models.Class", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UpdatedBy");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("Classes");
                });

            modelBuilder.Entity("JapaneseForum.Models.ClassStudent", b =>
                {
                    b.Property<string>("ClassId");

                    b.Property<string>("UserId");

                    b.HasKey("ClassId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("ClassStudent");
                });

            modelBuilder.Entity("JapaneseForum.Models.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("NTEXT");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<int>("PostId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UpdatedBy");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("PostId");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("JapaneseForum.Models.Labor", b =>
                {
                    b.Property<string>("WorkerId");

                    b.Property<string>("TimeTableId");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UpdatedBy");

                    b.HasKey("WorkerId", "TimeTableId");

                    b.HasAlternateKey("TimeTableId", "WorkerId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("Labors");
                });

            modelBuilder.Entity("JapaneseForum.Models.Media", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("VARCHAR(100)");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("Media");
                });

            modelBuilder.Entity("JapaneseForum.Models.Post", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<string>("Content")
                        .HasColumnType("NTEXT");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<int>("Status");

                    b.Property<string>("Thumbnail");

                    b.Property<string>("Title");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UpdatedBy");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("JapaneseForum.Models.Question", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AnswerA")
                        .IsRequired()
                        .HasColumnType("NTEXT");

                    b.Property<string>("AnswerB")
                        .IsRequired()
                        .HasColumnType("NTEXT");

                    b.Property<string>("AnswerC")
                        .IsRequired()
                        .HasColumnType("NTEXT");

                    b.Property<string>("AnswerD")
                        .IsRequired()
                        .HasColumnType("NTEXT");

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("NTEXT");

                    b.Property<int?>("MediaId");

                    b.Property<int>("RightAnswer");

                    b.Property<int>("TestId");

                    b.Property<int>("TypeId");

                    b.HasKey("Id");

                    b.HasIndex("MediaId");

                    b.HasIndex("TestId");

                    b.HasIndex("TypeId");

                    b.ToTable("Questions");
                });

            modelBuilder.Entity("JapaneseForum.Models.QuestionType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("QuestionTypes");
                });

            modelBuilder.Entity("JapaneseForum.Models.Test", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Status");

                    b.Property<int>("TestTime");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UpdatedBy");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("Tests");
                });

            modelBuilder.Entity("JapaneseForum.Models.TestAnswer", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<int>("QuestionId");

                    b.Property<int>("Answer");

                    b.HasKey("UserId", "QuestionId");

                    b.HasAlternateKey("QuestionId", "UserId");

                    b.ToTable("TestAnswers");
                });

            modelBuilder.Entity("JapaneseForum.Models.TestPermission", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<int>("TestId");

                    b.Property<bool>("IsAllowed");

                    b.HasKey("UserId", "TestId");

                    b.HasAlternateKey("TestId", "UserId");

                    b.ToTable("TestPermissions");
                });

            modelBuilder.Entity("JapaneseForum.Models.TimeTable", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClassId")
                        .IsRequired();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("Day");

                    b.Property<string>("Location");

                    b.Property<TimeSpan>("TimeEnd");

                    b.Property<TimeSpan>("TimeStart");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<string>("UpdatedBy");

                    b.HasKey("Id");

                    b.HasIndex("ClassId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("TimeTables");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("JapaneseForum.Models.Class", b =>
                {
                    b.HasOne("JapaneseForum.Models.ApplicationUser", "CreatedOwner")
                        .WithMany("CreatedClasses")
                        .HasForeignKey("CreatedBy");

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "UpdatedOwner")
                        .WithMany("UpdatedClasses")
                        .HasForeignKey("UpdatedBy");
                });

            modelBuilder.Entity("JapaneseForum.Models.ClassStudent", b =>
                {
                    b.HasOne("JapaneseForum.Models.Class", "Class")
                        .WithMany("ClassStudents")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "Student")
                        .WithMany("ClassStudents")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JapaneseForum.Models.Comment", b =>
                {
                    b.HasOne("JapaneseForum.Models.ApplicationUser", "CreatedOwner")
                        .WithMany("CreatedComments")
                        .HasForeignKey("CreatedBy");

                    b.HasOne("JapaneseForum.Models.Post", "Post")
                        .WithMany("Comments")
                        .HasForeignKey("PostId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "UpdatedOwner")
                        .WithMany("UpdatedComments")
                        .HasForeignKey("UpdatedBy");
                });

            modelBuilder.Entity("JapaneseForum.Models.Labor", b =>
                {
                    b.HasOne("JapaneseForum.Models.ApplicationUser", "CreatedOwner")
                        .WithMany("CreatedLabors")
                        .HasForeignKey("CreatedBy");

                    b.HasOne("JapaneseForum.Models.TimeTable", "TimeTable")
                        .WithMany("Labors")
                        .HasForeignKey("TimeTableId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "UpdatedOwner")
                        .WithMany("UpdatedLabors")
                        .HasForeignKey("UpdatedBy");

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "Worker")
                        .WithMany("Labors")
                        .HasForeignKey("WorkerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JapaneseForum.Models.Post", b =>
                {
                    b.HasOne("JapaneseForum.Models.Category", "Category")
                        .WithMany("Posts")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "CreatedOwner")
                        .WithMany("CreatedPosts")
                        .HasForeignKey("CreatedBy");

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "UpdatedOwner")
                        .WithMany("UpdatedPosts")
                        .HasForeignKey("UpdatedBy");
                });

            modelBuilder.Entity("JapaneseForum.Models.Question", b =>
                {
                    b.HasOne("JapaneseForum.Models.Media", "Media")
                        .WithMany("Questions")
                        .HasForeignKey("MediaId");

                    b.HasOne("JapaneseForum.Models.Test", "Test")
                        .WithMany("Questions")
                        .HasForeignKey("TestId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.QuestionType", "Type")
                        .WithMany("Questions")
                        .HasForeignKey("TypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JapaneseForum.Models.Test", b =>
                {
                    b.HasOne("JapaneseForum.Models.ApplicationUser", "CreatedOwner")
                        .WithMany("CreatedTests")
                        .HasForeignKey("CreatedBy");

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "UpdatedOwner")
                        .WithMany("UpdatedTests")
                        .HasForeignKey("UpdatedBy");
                });

            modelBuilder.Entity("JapaneseForum.Models.TestAnswer", b =>
                {
                    b.HasOne("JapaneseForum.Models.Question", "Question")
                        .WithMany()
                        .HasForeignKey("QuestionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JapaneseForum.Models.TestPermission", b =>
                {
                    b.HasOne("JapaneseForum.Models.Test", "Test")
                        .WithMany("TestPermission")
                        .HasForeignKey("TestId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "User")
                        .WithMany("TestPermisions")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("JapaneseForum.Models.TimeTable", b =>
                {
                    b.HasOne("JapaneseForum.Models.Class", "Class")
                        .WithMany("TimeTables")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "CreatedOwner")
                        .WithMany("CreatedTimeTables")
                        .HasForeignKey("CreatedBy");

                    b.HasOne("JapaneseForum.Models.ApplicationUser", "UpdatedOwner")
                        .WithMany("UpdatedTimeTables")
                        .HasForeignKey("UpdatedBy");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("JapaneseForum.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("JapaneseForum.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("JapaneseForum.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
