﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JapaneseForum.Data.Migrations
{
    public partial class UpdateReferenceBetweenFineAndClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fines_Classes_ClassId",
                table: "Fines");

            migrationBuilder.AddForeignKey(
                name: "FK_Fines_Classes_ClassId",
                table: "Fines",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Fines_Classes_ClassId",
                table: "Fines");

            migrationBuilder.AddForeignKey(
                name: "FK_Fines_Classes_ClassId",
                table: "Fines",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
