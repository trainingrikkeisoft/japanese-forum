﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using JapaneseForum.Repositories;
using JapaneseForum.Models.UserViewModels;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Const;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace JapaneseForum.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : BaseController
    {
        UserManager<ApplicationUser> _userManager;
        RoleManager<IdentityRole> _roleManager;
        private readonly IUserRepository _userRepository;
        private readonly ILogger _logger;

        public UserController(
            IUserRepository userRepository,
            UserManager<ApplicationUser> userManager,
            ILoggerFactory loggerFactory,
            RoleManager<IdentityRole> roleManager
            ) : base(userManager)
        {
            _roleManager = roleManager;
            _userRepository = userRepository;
            _logger = loggerFactory.CreateLogger<UserController>();
            _userManager = userManager;
        }

        // GET: User
        public ActionResult Index(IndeUserViewModel model)
        {
            var uses = _userRepository.All();
            model.Users = uses;
            return View(model);
        }

        // GET: User/Create
        [HttpGet]
        public ActionResult Create()
        {
            AddOrEditUserViewModel model = new AddOrEditUserViewModel();
            var roles = _roleManager.Roles;
            Dictionary<string, bool> roleListModel = new Dictionary<string, bool>();
            foreach (var item in roles)
            {
                roleListModel.Add(item.Name, false);
            }
            model.RoleList = roleListModel;
            return View(model);
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddOrEditUserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (await _userManager.FindByEmailAsync(model.Email) == null)
                    {
                        ApplicationUser user = new ApplicationUser
                        {
                            UserName = model.Email,
                            Email = model.Email,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            PhoneNumber = model.PhoneNumber,
                            Birthday = model.Birthday ?? DateTime.MinValue,
                            Gender = model.Gender
                        };
                        var result = await _userManager.CreateAsync(user);
                        if (result.Succeeded)
                        {
                            var savedUser = await _userManager.FindByNameAsync(model.Email);
                            var roleList = model.SelectedRoles;
                            foreach (var item in roleList)
                            {
                                var result1 = await _userManager.AddToRoleAsync(savedUser, item);
                                if (!result1.Succeeded)
                                {
                                    _logger.LogError("Add user error ", result1.Errors);
                                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                                    return View(model);
                                }
                            }
                            TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            _logger.LogError("", result);
                            TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                        }
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_ERROR_EMAIL_EXITS;
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                _logger.LogError("Add user error ", ex);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ex.Message;
                return View(model);
            }
        }

        // GET: User/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var roles = _roleManager.Roles;
            Dictionary<string, bool> roleListModel = new Dictionary<string, bool>();

            var rolesOfUser = await _userManager.GetRolesAsync(user);
            foreach (var item in roles)
            {
                if (rolesOfUser.Contains(item.Name))
                    roleListModel.Add(item.Name, true);
                else
                    roleListModel.Add(item.Name, false);
            }
            List<string> selectedRoles = new List<string>();
            foreach (var item in rolesOfUser)
            {
                selectedRoles.Add(item);
            }
            var model = new AddOrEditUserViewModel
            {
                Id = id,
                Birthday = user.Birthday,
                Email = user.Email,
                FirstName = user.FirstName,
                Gender = user.Gender,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                RoleList = roleListModel,
                SelectedRoles = selectedRoles
            };
            return View(model);
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, AddOrEditUserViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByIdAsync(id);
                    if (user != null)
                    {
                        user.UserName = model.Email;
                        user.FirstName = model.FirstName;
                        user.LastName = model.LastName;
                        user.PhoneNumber = model.PhoneNumber;
                        user.Birthday = model.Birthday ?? DateTime.MinValue;
                        user.Gender = model.Gender;
                        var roleList = model.SelectedRoles;
                        var roles = await _userManager.GetRolesAsync(user);
                        await _userManager.RemoveFromRolesAsync(user, roles.ToArray());
                        foreach (var item in roleList)
                        {
                            bool isInRole = await _userManager.IsInRoleAsync(user, item);
                            if (!isInRole)
                            {
                                var result1 = await _userManager.AddToRoleAsync(user, item);
                                if (!result1.Succeeded)
                                {
                                    _logger.LogError("Add user error ", result1.Errors);
                                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = "Cập nhật quyền lỗi.";
                                    break;
                                }
                            }
                        }
                        var result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_UPDATE_DATA;
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_UPDATE_DATA;
                        }
                    }
                    else
                    {
                        //Email exits
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_ERROR_DATA_NOT_EXITS;
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Update user error", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }

        // GET: User/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                // TODO: Add delete logic here
                var user = await _userManager.FindByIdAsync(id);
                if (user != null)
                {
                    var result = await _userManager.DeleteAsync(user);
                    if (result.Succeeded)
                    {
                        TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_DELETE_DATA;
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_DELETE_DATA;
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError("Delete user error", ex);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_DELETE_DATA;
                return RedirectToAction("Index");
            }
        }

        public ActionResult ChangeName()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangeName(ApplicationUser model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string userId = GetUserId();
                    ApplicationUser user = _userRepository.Find(userId);
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    int result = _userRepository.Update(user);
                    if (result == 1)
                    {
                        TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_UPDATE_DATA;
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_UPDATE_DATA;
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception e)
            {
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_UPDATE_DATA;
                _logger.LogError("Change user full name error", e);
            }
            return View(model);
        }
    }
}