﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Models.ClassViewModels;
using JapaneseForum.Repositories;
using JapaneseForum.Const;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using JapaneseForum.Models;
using JapaneseForum.Repositories.ClassStudent;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Models.User;
using JapaneseForum.Models.CalendarViewModels;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace JapaneseForum.Controllers
{
    [Authorize(Roles = "Admin, Teacher")]
    public class ClassController : BaseController
    {
        private readonly IClassRepository _classRepository;
        private readonly IClassStudentRepository _classStudentRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITimeTableRepository _timeTableRepository;
        private readonly ILogger _logger;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ClassController(
            IClassRepository classRepository,
            IClassStudentRepository classStudentRepository,
            IUserRepository userRepository,
            ITimeTableRepository timeTableRepository,
            ILoggerFactory loggerFactory,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager) : base(userManager)
        {
            _classRepository = classRepository;
            _classStudentRepository = classStudentRepository;
            _userRepository = userRepository;
            _timeTableRepository = timeTableRepository;
            _logger = loggerFactory.CreateLogger<ClassController>();
            _roleManager = roleManager;
        }

        // GET: Class
        public ActionResult Index(IndexClassViewModel model)
        {
            var allClass = _classRepository.All();
            foreach (var item in allClass)
            {
                model.Classes.Add(new ClassItemViewModel(item));
            }
            return View(model);
        }

        // GET: Class/Details/5
        public ActionResult Details(string id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListStudent(string id)
        {
            var _classInfo = _classRepository.All().SingleOrDefault(x => x.Id.Equals(id));
            var model = new StudentClassViewModel
            {
                Class = _classInfo
            };

            _classInfo.ClassStudents.ToList().ForEach(item => model.Students.Add(new UserItemViewModel(item.Student)));
            return View(model);
        }

        [HttpGet]
        public ActionResult AddStudent(string id)
        {
            var _classInfo = _classRepository.All().SingleOrDefault(x => x.Id.Equals(id));
            var _lstStudentIn = _classInfo.ClassStudents.Select(x => x.Student).ToList();
            var _lstStudentNotIn = _userRepository.All().Where(x => !_lstStudentIn.Any(n => n.Id.Equals(x.Id))).ToList();
            var model = new StudentClassViewModel
            {
                Class = _classInfo,
            };
            _lstStudentNotIn.ToList().ForEach(item => model.Students.Add(new UserItemViewModel(item)));
            return View(model);
        }

        [HttpPost]
        public ActionResult AddStudent(string id, string[] usersId)
        {
            if (usersId != null)
            {
                foreach (var userId in usersId)
                {
                    var _classStudent = new ClassStudent
                    {
                        ClassId = id,
                        UserId = userId
                    };
                    var rs = _classStudentRepository.Add(_classStudent);
                }
            }

            return RedirectToAction(nameof(ListStudent), new { id = id });
        }

        [HttpPost]
        public ActionResult RemoveStudent(string id, string[] usersId)
        {
            if (usersId != null && usersId.Length > 0)
            {
                foreach (var userId in usersId)
                {
                    var _classStudent = _classStudentRepository.Where(x => x.UserId.Equals(userId) && x.ClassId.Equals(id)).FirstOrDefault();
                    if (_classStudent != null)
                    {
                        var rs = _classStudentRepository.Delete(_classStudent);
                    }
                }
            }

            return RedirectToAction(nameof(ListStudent), new { id = id });
        }

        // GET: Class/Create
        public ActionResult Create()
        {
            CreateEditClassViewModel model = new CreateEditClassViewModel();
            var teachers = _userRepository.All()
                //.Where(i => i.Roles.Select(r => r.RoleId).Contains(ConstAuthozation.ROLE_TEACHER))
                .Select(i => new
                {
                    Id = i.Id,
                    Name = i.LastName + " " + i.FirstName
                })
                .ToList();

            model.TeacherList = new SelectList(teachers, "Id", "Name");
            return View(model);
        }

        // POST: Class/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateEditClassViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                var _class = new JapaneseForum.Models.Class
                {
                    Name = model.Name,
                    TeacherId = model.TeacherId,
                    CreatedAt = DateTime.Now,
                    CreatedBy = GetUserId()
                };
                var result = _classRepository.Add(_class);
                if (result == 1)
                {
                    TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                    return View();
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Create class error : ", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = e.Message;
                return View();
            }
        }

        // GET: Class/Edit/5
        public async Task<ActionResult> EditAsync(string id)
        {
            CreateEditClassViewModel model = null;
            var _class = _classRepository.Find(id);
            if (_class != null)
            {
                var roleTeacher = await _roleManager.FindByNameAsync(ConstAuthozation.ROLE_TEACHER);
                var teachers = _userRepository.All().Where(user => user.Roles.Select(r => r.RoleId).Contains(roleTeacher.Id)).ToList();
                teachers.Insert(0, new ApplicationUser() { Id = "", FirstName = "", LastName = "" });
                var teacherForSelect = teachers.Select(i => new
                {
                    Id = i.Id,
                    Name = i.LastName + " " + i.FirstName
                })
                    ;
                var selectList = new SelectList(teacherForSelect, "Id", "Name");
                model = new CreateEditClassViewModel(_class)
                {
                    TeacherList = selectList
                };
            }
            return View(model);
        }

        // POST: Class/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, CreateEditClassViewModel model)
        {
            try
            {
                // TODO: Add update logic here
                var _class = _classRepository.Find(id);
                if (_class != null)
                {
                    _class.Name = model.Name;
                    _class.UpdatedAt = DateTime.Now;
                    _class.UpdatedBy = GetUserId();
                    _class.TeacherId = model.TeacherId;
                    if (_classRepository.Update(_class) == 1)
                    {
                        TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_UPDATE_DATA;
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_UPDATE_DATA;
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogError("Update class error : ", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = e.Message;
                return View();
            }
        }

        // GET: Class/Delete/5
        public ActionResult Delete(string id)
        {
            try
            {
                var _class = _classRepository.SingleOrDefault(x => x.Id.Equals(id));
                if (_class != null)
                {
                    var result = _classRepository.Delete(_class);
                    if (result == 1)
                    {
                        TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_UPDATE_DATA;
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_UPDATE_DATA;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Delete class error : ", ex);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_SUCCESS_UPDATE_DATA;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Calendar(string id)
        {
            TimeTable classTimeTable = _timeTableRepository.Where(item => item.ClassId.Equals(id)).FirstOrDefault();
            Class currClass = _classRepository.Find(id);
            CalendarViewModel model = new CalendarViewModel();
            model.ClassId = id;
            model.Title = "Lịch học" + currClass?.Name;
            return View(model);
        }

        public string GetEvents(string classId)
        {
            CalendarEvents events = new CalendarEvents();
            List<TimeTable> timeList = _timeTableRepository.Where(i => i.ClassId.Equals(classId)).ToList();
            List<CalendarRange> ranges = new List<CalendarRange>();
            CalendarRange range = new CalendarRange();

            foreach (var time in timeList)
            {
                switch (time.RepeatType)
                {
                    case ConstRepeatType.NotRepeat:
                        if (time.IsAllDay)
                        {
                            events.Add(new CalendarEvent()
                            {
                                Id = time.Id,
                                Title = time.Title,
                                ClassName = time.Location,
                                Start = time.DateStart.ToString(),
                                End = time.DateEnd.ToString()
                            });
                        }
                        else
                        {
                            events.Add(new CalendarEvent()
                            {
                                Id = time.Id,
                                Title = time.Title,
                                ClassName = time.Location,
                                Start = time.DateStart.Add(time.TimeStart).ToString(),
                                End = time.DateEnd.Add(time.TimeEnd).ToString()
                            });
                        }
                        break;
                    case ConstRepeatType.Daily:
                        range = new CalendarRange(time.DateStart, time.DateEnd);
                        ranges = new List<CalendarRange>();
                        ranges.Add(range);
                        events.Add(new CalendarEvent()
                        {
                            Id = time.Id,
                            Title = time.Title,
                            ClassName = time.Location,
                            Start = time.TimeStart.ToString(),
                            End = time.TimeEnd.ToString(),
                            Repeat = ConstRepeatType.Daily,
                            Ranges = ranges
                        });
                        break;
                    case ConstRepeatType.Weekly:
                        range = new CalendarRange(time.DateStart, time.DateEnd);
                        ranges = new List<CalendarRange>();
                        range = new CalendarRange(time.DateStart, time.DateEnd);
                        ranges.Add(range);
                        string[] dowItem = time.RepeatNo.Split(new string[] { ", " }, StringSplitOptions.RemoveEmptyEntries);
                        events.Add(new CalendarEvent()
                        {
                            Id = time.Id,
                            Title = time.Title,
                            ClassName = time.Location,
                            Start = time.TimeStart.ToString(),
                            End = time.TimeEnd.ToString(),
                            Dow = dowItem,
                            Ranges = ranges
                        });
                        break;
                }
            }
            return events.Serialize();
        }

        [HttpPost]
        public JsonResult GetEvent(string id)
        {
            TimeTable time = _timeTableRepository.Find(id);
            if (time != null)
                return Json(time);
            else
                return null;
        }

        [HttpPost]
        public string AddCalendar(CalendarViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TimeTable timeTable = new TimeTable();
                    timeTable.Title = model.Title;
                    timeTable.ClassId = model.ClassId;
                    timeTable.CreatedAt = DateTime.Now;
                    timeTable.CreatedBy = GetUserId();
                    timeTable.DateStart = model.DateStart;
                    timeTable.IsAllDay = model.AllDay;
                    if (!model.AllDay)
                    {
                        timeTable.TimeStart = model.TimeStart ?? TimeSpan.MinValue;
                        timeTable.TimeEnd = model.TimeEnd ?? TimeSpan.MinValue;
                        timeTable.DateEnd = model.DateEnd;
                    }
                    else
                    {
                        timeTable.DateEnd = model.DateEnd.AddDays(1);
                    }

                    timeTable.RepeatType = model.RepeatType;
                    for (int i = 0; i < 7; i++)
                    {
                        timeTable.RepeatNo += model.Dow[i] ? i.ToString() + ", " : "";
                    }
                    timeTable.Location = model.Location;

                    var result = _timeTableRepository.Add(timeTable);
                    return result.ToString();
                }
                return getErrorFromModelState()?.First();
            }
            catch (Exception e)
            {
                _logger.LogError("Add calendar for class error", e.Message);
                return ConstResult.Failed.ToString();
            }
        }

        public string UpdateCalendar(CalendarViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    TimeTable timeTable = _timeTableRepository.Find(model.Id);
                    if (timeTable != null)
                    {
                        timeTable.Title = model.Title;
                        timeTable.ClassId = model.ClassId;
                        timeTable.UpdatedAt = DateTime.Now;
                        timeTable.UpdatedBy = GetUserId();
                        timeTable.DateStart = model.DateStart;
                        timeTable.DateEnd = model.DateEnd;
                        timeTable.IsAllDay = model.AllDay;
                        if (!model.AllDay)
                        {
                            timeTable.TimeStart = model.TimeStart ?? TimeSpan.MinValue;
                            timeTable.TimeEnd = model.TimeEnd ?? TimeSpan.MinValue;
                        }

                        timeTable.RepeatType = model.RepeatType;
                        timeTable.RepeatNo = string.Empty;
                        for (int i = 0; i < 7; i++)
                        {
                            timeTable.RepeatNo += model.Dow[i] ? i.ToString() + ", " : "";
                        }
                        timeTable.Location = model.Location;

                        var result = _timeTableRepository.Update(timeTable);
                        return result.ToString();
                    }
                }
                return getErrorFromModelState()?.First();
            }
            catch (Exception e)
            {
                _logger.LogError("Update calendar for class error", e.Message);
                return ConstResult.Failed.ToString();
            }
        }

        public int DeleteCalendar(CalendarViewModel model)
        {
            try
            {
                TimeTable deleteTime = _timeTableRepository.Find(model.Id);
                if (deleteTime != null)
                {
                    int result = _timeTableRepository.Delete(deleteTime);
                    return result;
                }
                else
                {
                    return ConstResult.Failed;
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Delete calendar id " + model.Id + " for class error", e.Message);
                return ConstResult.Failed;
            }
        }

        private List<string> getErrorFromModelState()
        {
            var modelErrors = new List<string>();
            foreach (var modelState in ModelState.Values)
            {
                foreach (var modelError in modelState.Errors)
                {
                    modelErrors.Add(modelError.ErrorMessage);
                }
            }
            return modelErrors;
        }
    }
}