using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Models;
using JapaneseForum.Const;

namespace JapaneseForum.Controllers
{
    public class BaseController : Controller
    {
        protected UserManager<ApplicationUser> _userManager;

        public BaseController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public string GetUserId()
        {
            var user = _userManager.GetUserId(User);
            return user;
        }

        public async Task<ApplicationUser> GetUserAsync()
        {
            ApplicationUser user = await _userManager.GetUserAsync(User);
            return user;
        }
    }
}