using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Models.FileUploadViewModels;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using JapaneseForum.Const;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace JapaneseForum.Controllers
{
    [Authorize]
    public class FileUploadController : Controller
    {
        private IHostingEnvironment _environment;
        private static Random random = new Random();
        private ILogger _logger;

        public FileUploadController(IHostingEnvironment environment,
             ILoggerFactory logger)
        {
            _environment = environment;
            _logger = logger.CreateLogger<FileUploadController>();
        }

        public IActionResult ImageUpload()
        {
            return View();
        }

        [HttpPost]
        public async Task<string> ImageUpload(ICollection<IFormFile> files)
        {
            try
            {
                IFormFile file = files.ElementAt(0);
                var uploadFolder = Path.Combine(_environment.WebRootPath, "uploads");
                string typeStr = string.Empty;
                string name = DateTime.Now.ToString("yyyyMMddhhmmss");
                if (file.Length > 0)
                {
                    typeStr = file.FileName.Substring(file.FileName.IndexOf("."));

                    using (var fileStream = new FileStream(Path.Combine(uploadFolder, name + typeStr ), FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
                return "http://" + Request.Host + "/uploads/" + name + typeStr;
            }
            catch (Exception e)
            {
                _logger.LogError("Upload image error", e);
                return ConstMessage.MESSAGE_FAILED_ADD_DATA;
            }
        }

        public IActionResult AudioUpload()
        {
            return View();
        }

        [HttpPost]
        public async Task<string> AudioUpload(ICollection<IFormFile> files)
        {
            try
            {
                IFormFile file = files.ElementAt(0);
                var uploadFolder = Path.Combine(_environment.WebRootPath, "uploads");
                string typeStr = string.Empty;
                string name = DateTime.Now.ToString("yyyyMMddhhmmss");
                if (file.Length > 0)
                {
                    typeStr = file.FileName.Substring(file.FileName.LastIndexOf("."));

                    using (var fileStream = new FileStream(Path.Combine(uploadFolder, name + typeStr), FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
                return "http://" + Request.Host + "/uploads/" + name + typeStr;
            }
            catch (Exception e)
            {
                _logger.LogError("Upload image error", e);
                return ConstMessage.MESSAGE_FAILED_ADD_DATA;
            }
        }
    }
}