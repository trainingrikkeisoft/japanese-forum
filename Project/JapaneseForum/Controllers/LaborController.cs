using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Models.CalendarViewModels;
using JapaneseForum.Models.LaborViewModels;
using JapaneseForum.Const;
using Microsoft.AspNetCore.Authorization;
using JapaneseForum.Repositories;

namespace JapaneseForum.Controllers
{
    [Authorize(Roles = "Admin, Teacher, President")]
    public class LaborController : Controller
    {
        ILaborRepository _laborRepository;

        public LaborController(ILaborRepository laboRepository)
        {
            _laborRepository = laboRepository;
        }

        // GET: Labor
        public ActionResult Index()
        {
            LaborIndexViewModel model = new LaborIndexViewModel();
            CalendarEvents events = new CalendarEvents();
            if (User.IsInRole(ConstAuthozation.ROLE_ADMIN))
            {
                var labors = _laborRepository.All();
            }
            else if (User.IsInRole(ConstAuthozation.ROLE_PRESIDENT))
            {
                var labors = _laborRepository.Where(item => item.WorkerId != null );
            }
            else if (User.IsInRole(ConstAuthozation.ROLE_TEACHER))
            {

            }
            TimeSpan time = new TimeSpan(0, 0, 70, 0);
            events.Add(new CalendarEvent()
            {
                Id = 1,
                Title = "One time Today",
                Start = DateTime.Now.ToString(),
                End = DateTime.Now.Add(time).ToString()
            });

            events.Add(new CalendarEvent()
            {
                Id = 1,
                Title = "All day",
                Start = DateTime.Today.ToString(),
                End = DateTime.Today.ToString()
            });

            var ranges = new List<CalendarRange>();
            CalendarRange range = new CalendarRange(DateTime.Now, DateTime.Now.Add(new TimeSpan(14, 0, 0, 0)));
            ranges.Add(range);
            events.Add(new CalendarEvent()
            {
                Id = 2,
                Title = "Repeat Weekly Monday",
                Start = TimeSpan.Parse("10:00").ToString(),
                End = TimeSpan.Parse("12:00").ToString(),
                Dow = new int[1] { 1 },
                Ranges = ranges
            });

            ranges = new List<CalendarRange>();
            range = new CalendarRange(DateTime.Now, DateTime.Now.Add(new TimeSpan(144, 0, 0, 0)));
            ranges.Add(range);
            events.Add(new CalendarEvent()
            {
                Id = 3,
                Title = "Repeat monthly day 2",
                Start = "2017-08-01",
                End = "2017-08-02",
                //Dow = new int[1] { 2 },
                //Ranges = ranges,
                Repeat = 1
            });
            
            //ranges = new List<CalendarRange>();
            //range = new CalendarRange(DateTime.Now, DateTime.Now.Add(new TimeSpan(144, 0, 0, 0)));
            //ranges.Add(range);
            //events.Add(new CalendarEvent()
            //{
            //    Id = 2,
            //    Title = "Repeat yearly 13-8",
            //    Start = TimeSpan.Parse("10:00").ToString(),
            //    End = TimeSpan.Parse("12:00").ToString(),
            //    Dow = new int[1] { 1 },
            //    Ranges = ranges,
            //    Repeat = 2
            //});
            model.CalendarEvents = events;
            return View(model);
        }

        // GET: Labor/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Labor/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Labor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Labor/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Labor/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Labor/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Labor/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public string GetEvents()
        {
            CalendarEvents events = new CalendarEvents();
            if (User.IsInRole(ConstAuthozation.ROLE_ADMIN))
            {
                var labors = _laborRepository.All();
            }
            else if (User.IsInRole(ConstAuthozation.ROLE_PRESIDENT))
            {
                var labors = _laborRepository.Where(item => item.WorkerId != null);
            }
            else if (User.IsInRole(ConstAuthozation.ROLE_TEACHER))
            {

            }
            TimeSpan time = new TimeSpan(0, 0, 70, 0);
            events.Add(new CalendarEvent()
            {
                Id = 1,
                Title = "One time Today",
                Start = DateTime.Now.ToString(),
                End = DateTime.Now.Add(time).ToString()
            });

            events.Add(new CalendarEvent()
            {
                Id = 1,
                Title = "All day",
                Start = DateTime.Today.ToString(),
                End = DateTime.Today.ToString()
            });

            var ranges = new List<CalendarRange>();
            CalendarRange range = new CalendarRange(DateTime.Now, DateTime.Now.Add(new TimeSpan(21, 0, 0, 0)));
            ranges.Add(range);
            events.Add(new CalendarEvent()
            {
                Id = 2,
                Title = "Repeat Weekly Monday",
                Start = TimeSpan.Parse("10:00").ToString(),
                End = TimeSpan.Parse("12:00").ToString(),
                Dow = new int[1] { 1 },
                Ranges = ranges
            });

            ranges = new List<CalendarRange>();
            range = new CalendarRange(DateTime.Now, DateTime.Now.Add(new TimeSpan(144, 0, 0, 0)));
            ranges.Add(range);
            events.Add(new CalendarEvent()
            {
                Id = 2,
                Title = "Repeat monthly day 2",
                Start = "0000-08-01T00:00:00",
                //Dow = new int[1] { 2 },
                //Ranges = ranges,
                Repeat = 1
            });

            //ranges = new List<CalendarRange>();
            //range = new CalendarRange(DateTime.Now, DateTime.Now.Add(new TimeSpan(144, 0, 0, 0)));
            //ranges.Add(range);
            //events.Add(new CalendarEvent()
            //{
            //    Id = 2,
            //    Title = "Repeat yearly 13-8",
            //    Start = TimeSpan.Parse("10:00").ToString(),
            //    End = TimeSpan.Parse("12:00").ToString(),
            //    Dow = new int[1] { 1 },
            //    Ranges = ranges,
            //    Repeat = 2
            //});



            return events.Serialize();
        }

        public ActionResult TestTest()
        {
            return View();
        }
    }
}