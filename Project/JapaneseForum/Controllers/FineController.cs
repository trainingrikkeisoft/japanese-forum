﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Repositories;
using Microsoft.Extensions.Logging;
using JapaneseForum.Models.FineViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using JapaneseForum.Models.ClassViewModels;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Const;
using Microsoft.AspNetCore.Authorization;

namespace JapaneseForum.Controllers
{
    [Authorize(Roles = "Admin, Teacher, President")]
    public class FineController : BaseController
    {
        private IFineRepository _fine;
        private ILogger _logger;
        private IClassRepository _class;
        private IUserRepository _user;

        public FineController(IFineRepository fine, 
            ILoggerFactory logger, 
            IClassRepository classRepo,
            IUserRepository user,
            UserManager<ApplicationUser> usermanager) : base(usermanager)
        {
            _fine = fine;
            _logger = logger.CreateLogger<FineController>();
            _class = classRepo;
            _user = user;
        }

        // GET: Fine
        public ActionResult Index(string classId)
        {
            IndexClassViewModel model = new IndexClassViewModel();
            model.Classes = new List<ClassItemViewModel>();
            var classList = _class.All().ToList();
            classList.ForEach(i => model.Classes.Add(new ClassItemViewModel(i)));
            return View(model);
        }

        // GET: Fine/Create
        public ActionResult Create(string classId)
        {
            FineItemViewModel model = new FineItemViewModel();
            var studentIds = _class.Find(classId).ClassStudents.Select(i => i.UserId);
            var students = _user.Where(i => studentIds.Contains(i.Id)).Select(s => new
            {
                value = s.Id,
                text = s.LastName + s.FirstName
            }).ToList();
            model.StudentList = new SelectList(students, "value", "text");
            model.ClassId = classId;
            return View(model);
        }

        // POST: Fine/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FineItemViewModel model)
        {
            try
            {
                Fine fine = new Fine();
                fine.ClassId = model.ClassId;
                fine.StudentId = model.StudentId;
                fine.Reason = model.Reason;
                fine.IsPay = model.IsPay;
                fine.CreatedAt = DateTime.Now;
                fine.CreatedBy = GetUserId();
                fine.AmountOfMoney = model.AmountOfMoney;
                if (_fine.Add(fine) == 1)
                {
                    TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                    return RedirectToAction("List", new { classId = model.ClassId });
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Add fine error", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }

        // GET: Fine/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Fine/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogError("Delete fine error", e);
                return View();
            }
        }

        // GET: Fine
        public ActionResult List(string classId, SortOrder sortOrder)
        {
            var fineItemVMList = new List<FineItemViewModel>();
            var fines = _fine.Where(i => i.ClassId.Equals(classId)).OrderByDescending(i => i.IsPay).ToList();
            var prevSortType = TempData["prevsortType"] == null ? true : (bool)TempData["prevsortType"];
            var prevSort = TempData["prevSort"] == null ? SortOrder.None : (SortOrder)TempData["prevSort"];
            if (prevSort == sortOrder)
            {
                prevSortType = !prevSortType;
            }
            sortOrder = sortOrder == SortOrder.None ? prevSort : sortOrder;
            if (prevSortType)
            {
                switch (sortOrder)
                {
                    case SortOrder.Status:
                        fines = fines.OrderBy(i => i.IsPay).ToList();
                        break;
                    case SortOrder.CreatedAt:
                        fines = fines.OrderBy(i => i.CreatedAt).ToList();
                        break;
                    case SortOrder.Name:
                        fines = fines.OrderBy(i => i.StudentId).ToList();
                        break;
                    default:
                        fines = fines.OrderByDescending(i => i.CreatedAt).ToList();
                        break;

                }
            }
            else
            {
                switch (sortOrder)
                {
                    case SortOrder.Status:
                        fines = fines.OrderByDescending(i => i.IsPay).ToList();
                        break;
                    case SortOrder.CreatedAt:
                        fines = fines.OrderByDescending(i => i.CreatedAt).ToList();
                        break;
                    case SortOrder.Name:
                        fines = fines.OrderByDescending(i => i.StudentId).ToList();
                        break;
                    default:
                        fines = fines.OrderByDescending(i => i.CreatedAt).ToList();
                        break;
                }
            }
            IndexFineViewModel model = new IndexFineViewModel();
            fines.ForEach(i => fineItemVMList.Add(new FineItemViewModel(i)));
            model.Class = _class.Find(classId);
            model.FineList = fineItemVMList;
            TempData["prevsortType"] = prevSortType;
            TempData["prevSort"] = sortOrder;
            return View(model);
        }
        
        public ActionResult PayFine(int id)
        {
            try
            {
                Fine fine = _fine.Find(id);
                fine.IsPay = !fine.IsPay;
                if (_fine.Update(fine) == 1)
                {
                    TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                    return RedirectToAction("List", new { classId = fine.ClassId });
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                    return RedirectToAction("List", new { classId = fine.ClassId });
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Pay fine error", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }
    }
}