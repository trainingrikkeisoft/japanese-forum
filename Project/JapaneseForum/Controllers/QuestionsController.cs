﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Models.QuestionViewModels;
using JapaneseForum.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using JapaneseForum.Classes;
using JapaneseForum.Models;
using Microsoft.Extensions.Logging;
using JapaneseForum.Const;
using Microsoft.AspNetCore.Authorization;

namespace JapaneseForum.Controllers
{
    [Authorize]
    public class QuestionsController : Controller
    {
        IQuestionTypeRepository _questionTypeRespository;
        IQuestionRepository _questionRespository;
        ITestRepository _testRepository;
        private readonly ILogger _logger;

        /// <summary>
        /// save temp list of question of current test
        /// </summary>
        public List<CreateQuestionViewModel> _tempQuestionList = new List<CreateQuestionViewModel>();

        /// <summary>
        /// index of current question
        /// </summary>
        int _tempCurrentKey = 0;

        /// <summary>
        /// id of current test
        /// </summary>
        int _tempCurrentTest = 0;

        public QuestionsController(
            IQuestionTypeRepository questionTypeRespository,
            IQuestionRepository questionRespository,
            ILoggerFactory loggerFactory,
            ITestRepository testRepository
            )
        {
            _questionTypeRespository = questionTypeRespository;
            _questionRespository = questionRespository;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _testRepository = testRepository;
        }
        
        // GET: Questions/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Questions/Create
        [Authorize(Roles = "Teacher")]
        public ActionResult Create(int testId)
        {
            _tempCurrentTest = TempData["_tempCurrentTest"] == null ? 0 : (int)TempData["_tempCurrentTest"];
            if (_tempCurrentTest != testId)
            {
                _tempCurrentTest = testId;
                _tempQuestionList = new List<CreateQuestionViewModel>();
                var existQuestion = _questionRespository.Where(x => x.TestId == testId).OrderBy(i => i.Id).ToList();
                foreach (var item in existQuestion)
                {
                    _tempQuestionList.Add(new CreateQuestionViewModel
                    {
                        Id = item.Id,
                        No = _tempCurrentKey,
                        AnswerA = item.AnswerA,
                        AnswerB = item.AnswerB,
                        AnswerC = item.AnswerC,
                        AnswerD = item.AnswerD,
                        RightAnswer = (AnswerEnum)item.RightAnswer,
                        Content = item.Content,
                        TestID = _tempCurrentTest,
                        Type = item.TypeId
                    });
                    _tempCurrentKey++;
                }
                if (_tempCurrentKey > 0)
                    _tempCurrentKey--;
            }
            else
            {
                _tempCurrentKey = (int)TempData["_tempCurrentKey"];
                _tempQuestionList = TempData.Get<List<CreateQuestionViewModel>>("_tempQuestionList");
            }

            CreateQuestionViewModel model = new CreateQuestionViewModel();
            if (_tempQuestionList.Count > _tempCurrentKey)
            {
                model = _tempQuestionList[_tempCurrentKey];
            }
            model.No = _tempCurrentKey;
            model.TestID = testId;
            model.QuestionTypeList = new SelectList(_questionTypeRespository.All(), "Id", "Name");
            model.AnwerList = new SelectList((new AnswerCollection()).Collection, "Id", "Name");

            var currentTest = _testRepository.Find(_tempCurrentTest);
            ViewBag.TestName = currentTest == null ? "" : currentTest.Name;
            ViewBag.QuestionNoList = _tempQuestionList.Select(i => i.No + 1).ToList();
            TempData["_tempCurrentTest"] = _tempCurrentTest;
            TempData["_tempCurrentKey"] = _tempCurrentKey;
            TempData.Put("_tempQuestionList", _tempQuestionList);

            return View(model);
        }

        // POST: Questions/Create
        [Authorize(Roles = "Teacher")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateQuestionViewModel model)
        {
            try
            {
                _tempCurrentKey = (int)TempData["_tempCurrentKey"];
                _tempQuestionList = TempData.Get<List<CreateQuestionViewModel>>("_tempQuestionList");
                _tempCurrentTest = (int)TempData["_tempCurrentTest"];
                switch (Request.Form["BtnSubmit"])
                {
                    case "Hoàn thành và lưu":
                        if (ModelState.IsValid)
                        {
                            if (_tempQuestionList.Count > _tempCurrentKey)
                            {
                                _tempQuestionList[_tempCurrentKey] = model;
                            }
                            else
                            {
                                _tempQuestionList.Add(model);
                            }

                            var existQuestion = _questionRespository.Where(x => x.TestId == _tempCurrentTest).OrderBy(i => i.Id).ToList();
                            foreach (var item in _tempQuestionList)
                            {
                                if (existQuestion.Select(i => i.Id).Contains(item.Id))
                                {
                                    Question question = _questionRespository.Find(item.Id);
                                    question.AnswerA = item.AnswerA;
                                    question.AnswerB = item.AnswerB;
                                    question.AnswerC = item.AnswerC;
                                    question.AnswerD = item.AnswerD;
                                    question.RightAnswer = (int)item.RightAnswer;
                                    question.Content = item.Content;
                                    question.TestId = _tempCurrentTest;
                                    question.TypeId = item.Type;
                                    _questionRespository.Update(question);
                                }
                                else
                                {
                                    Question question = new Question()
                                    {
                                        AnswerA = item.AnswerA,
                                        AnswerB = item.AnswerB,
                                        AnswerC = item.AnswerC,
                                        AnswerD = item.AnswerD,
                                        RightAnswer = (int)item.RightAnswer,
                                        Content = item.Content,
                                        TestId = _tempCurrentTest,
                                        TypeId = item.Type,
                                    };
                                    _questionRespository.Add(question);
                                }
                            }
                            TempData["_tempQuestionList"] = null;
                            TempData["_tempCurrentKey"] = 0;
                            TempData["_tempCurrentTest"] = 0;
                            return RedirectToAction("Index", "Tests");
                        }
                        break;
                    case "Cập nhật":
                        if (ModelState.IsValid)
                        {
                            if (_tempQuestionList.Count > _tempCurrentKey)
                            {
                                _tempQuestionList[_tempCurrentKey] = model;
                            }
                            else
                            {
                                model.No = _tempCurrentKey;
                                _tempQuestionList.Add(model);
                            }
                        }
                        break;
                    case "Thêm câu hỏi mới":
                        if (ModelState.IsValid)
                        {
                            if (_tempQuestionList.Count > _tempCurrentKey)
                            {
                                _tempQuestionList[_tempCurrentKey] = model;
                            }
                            else
                            {
                                model.No = _tempCurrentKey;
                                _tempQuestionList.Add(model);
                            }
                            _tempCurrentKey = _tempQuestionList.Count;
                        }
                        break;
                    default:
                        string questionNoSTr = Request.Form["BtnSubmit"];
                        int questionNo = 0;
                        if (Int32.TryParse(questionNoSTr, out questionNo))
                            _tempCurrentKey = questionNo - 1;
                        else
                            _tempCurrentKey = 0;
                        break;
                }

                TempData["_tempCurrentKey"] = _tempCurrentKey;
                TempData["_tempCurrentTest"] = _tempCurrentTest;
                TempData.Put("_tempQuestionList", _tempQuestionList);
                return RedirectToAction("Create", new { testId = _tempCurrentTest });
            }
            catch (Exception e)
            {
                _logger.LogError("Add Question Error : ", e);
                return View();
            }
        }
        
        public ActionResult Answer(int testId)
        {
            try
            {
                _tempCurrentTest = TempData["_tempCurrentTest"] == null ? 0 : (int)TempData["_tempCurrentTest"];
                if (_tempCurrentTest != testId)
                {
                    _tempCurrentTest = testId;
                    _tempQuestionList = new List<CreateQuestionViewModel>();
                    var existQuestion = _questionRespository.Where(x => x.TestId == testId).OrderBy(i => i.Id).ToList();
                    foreach (var item in existQuestion)
                    {
                        _tempQuestionList.Add(new CreateQuestionViewModel
                        {
                            Id = item.Id,
                            No = _tempCurrentKey,
                            AnswerA = item.AnswerA,
                            AnswerB = item.AnswerB,
                            AnswerC = item.AnswerC,
                            AnswerD = item.AnswerD,
                            RightAnswer = (AnswerEnum)item.RightAnswer,
                            Content = item.Content,
                            TestID = _tempCurrentTest,
                            Type = item.TypeId
                        });
                        _tempCurrentKey++;
                    }
                    if (_tempCurrentKey > 0)
                        _tempCurrentKey--;
                }
                else
                {
                    _tempCurrentKey = (int)TempData["_tempCurrentKey"];
                    _tempQuestionList = TempData.Get<List<CreateQuestionViewModel>>("_tempQuestionList");
                }

                CreateQuestionViewModel model = new CreateQuestionViewModel();
                if (_tempQuestionList.Count > _tempCurrentKey)
                {
                    model = _tempQuestionList[_tempCurrentKey];
                }
                model.No = _tempCurrentKey;
                model.TestID = testId;
                model.QuestionTypeList = new SelectList(_questionTypeRespository.All(), "Id", "Name");
                model.AnwerList = new SelectList((new AnswerCollection()).Collection, "Id", "Name");

                ViewBag.TestName = _testRepository.Find(_tempCurrentTest).Name;
                ViewBag.QuestionNoList = _tempQuestionList.Select(i => i.No + 1).ToList();
                TempData["_tempCurrentTest"] = _tempCurrentTest;
                TempData["_tempCurrentKey"] = _tempCurrentKey;
                TempData.Put("_tempQuestionList", _tempQuestionList);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}