﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Models.PostViewModels;
using JapaneseForum.Repositories;
using System.Linq;
using JapaneseForum.Const;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using JapaneseForum.Authorization;
using JapaneseForum.Models.HomeViewModels;

namespace JapaneseForum.Controllers
{
    public class HomeController : BaseController
    {
        private IPostRepository _post;
        private readonly ILogger _logger;
        private readonly int pageSize = 5;
        private IAuthorizationService _authorizationService;

        public HomeController(UserManager<ApplicationUser> userManager, 
            IPostRepository post, 
            ILoggerFactory loggerFactory,
            IAuthorizationService authorizationService) : base(userManager)
        {
            _post = post;
            _userManager = userManager;
            _logger = loggerFactory.CreateLogger<HomeController>();
            _authorizationService = authorizationService;
        }

        public IActionResult Index()
        {
            HomeIndexViewModel model = new HomeIndexViewModel();
            List<CategoryWithPostsViewModel> listCateWithPosts = new List<CategoryWithPostsViewModel>();
            List<Post> postList = new List<Post>();
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            CategoryWithPostsViewModel cateWithPOsts = new CategoryWithPostsViewModel();

            postList = _post.All().Where(i => i.Category.Name.Equals(ConstPostCategory.Lesson)).OrderByDescending(i => i.CreatedAt).Where(i => i.Status == (int)StatusEnum.Active).Take(pageSize).ToList();
            cateWithPOsts.CategoryName = ConstPostCategory.Lesson;
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            cateWithPOsts.ListTopPosts = postViewModelList;
            listCateWithPosts.Add(cateWithPOsts);

            cateWithPOsts = new CategoryWithPostsViewModel();
            postList = new List<Post>();
            postViewModelList = new List<PostViewModel>();
            postList = _post.All().Where(i => i.Category.Name.Equals(ConstPostCategory.EntertainmentAndNews)).OrderByDescending(i => i.CreatedAt).Where(i => i.Status == (int)StatusEnum.Active).Take(pageSize).ToList();
            cateWithPOsts.CategoryName = ConstPostCategory.EntertainmentAndNews;
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            cateWithPOsts.ListTopPosts = postViewModelList;
            listCateWithPosts.Add(cateWithPOsts);

            cateWithPOsts = new CategoryWithPostsViewModel();
            postList = new List<Post>();
            postViewModelList = new List<PostViewModel>();
            postList = _post.All().Where(i => i.Category.Name.Equals(ConstPostCategory.TeacherAsk)).OrderByDescending(i => i.CreatedAt).Where(i => i.Status == (int)StatusEnum.Active).Take(pageSize).ToList();
            cateWithPOsts.CategoryName = ConstPostCategory.TeacherAsk;
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            cateWithPOsts.ListTopPosts = postViewModelList;
            listCateWithPosts.Add(cateWithPOsts);

            cateWithPOsts = new CategoryWithPostsViewModel();
            postList = new List<Post>();
            postViewModelList = new List<PostViewModel>();
            postList = _post.All().Where(i => i.Category.Name.Equals(ConstPostCategory.Document)).OrderByDescending(i => i.CreatedAt).Where(i => i.Status == (int)StatusEnum.Active).Take(pageSize).ToList();
            cateWithPOsts.CategoryName = ConstPostCategory.Document;
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            cateWithPOsts.ListTopPosts = postViewModelList;
            listCateWithPosts.Add(cateWithPOsts);
            
            cateWithPOsts = new CategoryWithPostsViewModel();
            postList = new List<Post>();
            postViewModelList = new List<PostViewModel>();
            postList = _post.All().OrderByDescending(i => i.TimeRead).Where(i => i.Status == (int)StatusEnum.Active).Take(pageSize).ToList();
            cateWithPOsts.CategoryName = "Bài đọc nhiều";
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            cateWithPOsts.ListTopPosts = postViewModelList;
            model.MostReadPosts = cateWithPOsts;


            cateWithPOsts = new CategoryWithPostsViewModel();
            postList = new List<Post>();
            postViewModelList = new List<PostViewModel>();
            postList = _post.All().OrderByDescending(i => i.CreatedAt).Where(i => i.Status == (int)StatusEnum.Active).Take(3).ToList();
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            cateWithPOsts.ListTopPosts = postViewModelList;
            model.CarouselPosts = cateWithPOsts;

            model.ListCategoryWithPosts = listCateWithPosts;
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }
        
    }
}
