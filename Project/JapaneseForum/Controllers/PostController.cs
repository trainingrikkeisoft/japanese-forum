using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Repositories;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Models;
using JapaneseForum.Models.PostViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using JapaneseForum.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using JapaneseForum.Const;

namespace JapaneseForum.Controllers
{
    [Authorize]
    public class PostController : BaseController
    {
        private UserManager<ApplicationUser> _usermanager;
        private ICategoryRepository _category;
        private IPostRepository _post;
        private ICommentRepository _comment;
        private readonly ILogger _logger;
        private readonly int pageSize = 5;

        public PostController(ICategoryRepository category,
            IPostRepository post,
            UserManager<ApplicationUser> usermanager,
            ILoggerFactory loggerFactory,
            ICommentRepository comment) : base(usermanager)
        {
            _category = category;
            _post = post;
            _usermanager = usermanager;
            System.Security.Claims.ClaimsPrincipal currentUser = User;
            _logger = loggerFactory.CreateLogger<PostController>();
            _comment = comment;
        }

        // GET: Post
        public ActionResult Index(SortOrder sortOrder, int page)
        {
            List<Post> postList = _post.All().ToList();
            var skip = pageSize * (page - 1);
            var canPage = skip < postList.Count;
            var prevSortType = TempData["prevsortType"] == null ? true : (bool)TempData["prevsortType"];
            var prevSort = TempData["prevSort"] == null ? SortOrder.None : (SortOrder)TempData["prevSort"];
            var prevPage = TempData["prevPage"] == null ? 0 : (int)TempData["prevPage"];
            if (prevSort == sortOrder)
            {
                prevSortType = !prevSortType;
            }
            sortOrder = sortOrder == SortOrder.None ? prevSort : sortOrder;
            page = page == 0 ? (prevPage == 0 ? 1 : prevPage) : page;
            if (prevSortType)
            {
                switch (sortOrder)
                {
                    case SortOrder.Id:
                        postList = postList.OrderBy(i => i.Id).ToList();
                        break;
                    case SortOrder.Status:
                        postList = postList.OrderBy(i => i.Status).ToList();
                        break;
                    case SortOrder.CreatedAt:
                        postList = postList.OrderBy(i => i.CreatedAt).ToList();
                        break;
                    case SortOrder.CreatedBy:
                        postList = postList.OrderBy(i => i.CreatedBy).ToList();
                        break;
                    case SortOrder.Category:
                        postList = postList.OrderBy(i => i.CategoryId).ToList();
                        break;
                    default:
                        postList = postList.OrderByDescending(i => i.CreatedAt).ToList();
                        break;

                }
            }
            else
            {
                switch (sortOrder)
                {
                    case SortOrder.Id:
                        postList = postList.OrderByDescending(i => i.Id).ToList();
                        break;
                    case SortOrder.Status:
                        postList = postList.OrderByDescending(i => i.Status).ToList();
                        break;
                    case SortOrder.CreatedAt:
                        postList = postList.OrderByDescending(i => i.CreatedAt).ToList();
                        break;
                    case SortOrder.CreatedBy:
                        postList = postList.OrderByDescending(i => i.CreatedBy).ToList();
                        break;
                    case SortOrder.Category:
                        postList = postList.OrderByDescending(i => i.CategoryId).ToList();
                        break;
                    default:
                        postList = postList.OrderByDescending(i => i.CreatedAt).ToList();
                        break;
                }
            }
            if (canPage)
            {
                postList = postList.Skip(skip).Take(pageSize).ToList();
            }
            IndexPostViewModel model = new IndexPostViewModel();
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));
            model.FullListPost = _post.All().ToList();
            model.ListPost = postViewModelList;
            model.PageSize = pageSize;
            model.CurrentPage = page;
            TempData["prevsortType"] = prevSortType;
            TempData["prevSort"] = sortOrder;
            TempData["prevPage"] = prevPage;
            return View(model);
        }

        // GET: Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            Post post = _post.FindIncludeComments(id);
            post.TimeRead++;
            _post.Update(post);
            PostViewModel model = new PostViewModel(post);
            return View(model);
        }

        // GET: Post/Create
        public ActionResult Create()
        {
            var model = new CreatePostViewModel();
            model.CategoryList = new SelectList(_category.All(), "Id", "Name");
            model.StatusList = new SelectList((new StatusCollection()).Collection, "Id", "Name");
            return View(model);
        }

        // POST: Post/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreatePostViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ViewData["Message"] = "CreatePost.";
                    Post post = new Post()
                    {
                        Title = model.Title,
                        Content = model.Content,
                        Status = (int)model.Status,
                        CategoryId = model.CategoryId,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        CreatedBy = GetUserId()
                    };
                    var result = _post.Add(post);
                    if (result == 1)
                    {
                        TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                        return View(model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Add post error", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }

        // GET: Post/Edit/5
        public ActionResult Edit(int id)
        {
            Post post = _post.Find(id);
            if (post == null)
                return NotFound();
            var model = new CreatePostViewModel(post);
            model.CategoryList = new SelectList(_category.All(), "Id", "Name");
            model.StatusList = new SelectList((new StatusCollection()).Collection, "Id", "Name");
            return View(model);
        }

        // POST: Post/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CreatePostViewModel model)
        {
            try
            {
                Post post = _post.Find(id);
                post.CategoryId = model.CategoryId;
                post.Content = model.Content;
                post.Title = model.Title;
                post.Status = (int)model.Status;
                post.UpdatedAt = DateTime.Now;
                post.UpdatedBy = GetUserId();
                var result = _post.Update(post);

                if (result == 1)
                {
                    TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Update Post error", e.Message);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }

        // GET: Post/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                if (_post.RemoveWhere(col => col.Id == id) == 1)
                {
                    TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_DELETE_DATA;
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_DELETE_DATA;
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_DELETE_DATA;
                _logger.LogError("Delete post error", e);
                return View();
            }
        }

        // GET: Learn
        [Route("Learn")]
        [AllowAnonymous]
        public ActionResult Learn(int page)
        {
            var pageSize = 10;
            List<Post> postList = _post
                .Where(i => i.Category.Name.Equals(ConstPostCategory.Lesson) && i.Status == (int)StatusEnum.Active)
                .OrderByDescending(i => i.CreatedAt).ToList();
            IndexPostViewModel model = new IndexPostViewModel()
            {
                FullListPost = postList
            };
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));

            var prevPage = TempData["prevPage"] == null ? 0 : (int)TempData["prevPage"];
            var skip = pageSize * (page - 1);
            var canPage = skip < postList.Count;
            page = page == 0 ? (prevPage == 0 ? 1 : prevPage) : page;
            if (canPage)
            {
                postList = postList.Skip(skip).Take(pageSize).ToList();
            }
            model.ListPost = postViewModelList;
            model.PageSize = pageSize;
            model.CurrentPage = page;
            TempData["prevPage"] = prevPage;
            return View(model);
        }

        // GET: Entertainment
        [Route("Entertainment")]
        [AllowAnonymous]
        public ActionResult Entertainment(int page)
        {
            var pageSize = 10;
            List<Post> postList = _post
                .Where(i => i.Category.Name.Equals(ConstPostCategory.EntertainmentAndNews) && i.Status == (int)StatusEnum.Active)
                .OrderByDescending(i => i.CreatedAt).ToList();
            IndexPostViewModel model = new IndexPostViewModel()
            {
                FullListPost = postList
            };
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));

            var prevPage = TempData["prevPage"] == null ? 0 : (int)TempData["prevPage"];
            var skip = pageSize * (page - 1);
            var canPage = skip < postList.Count;
            page = page == 0 ? (prevPage == 0 ? 1 : prevPage) : page;
            if (canPage)
            {
                postList = postList.Skip(skip).Take(pageSize).ToList();
            }
            model.ListPost = postViewModelList;
            model.PageSize = pageSize;
            model.CurrentPage = page;
            TempData["prevPage"] = prevPage;
            return View(model);
        }

        // GET: Learn
        [Route("Document")]
        [AllowAnonymous]
        public ActionResult Document(int page)
        {
            var pageSize = 10;
            List<Post> postList = _post
                .Where(i => i.Category.Name.Equals(ConstPostCategory.Document) && i.Status == (int)StatusEnum.Active)
                .OrderByDescending(i => i.CreatedAt).ToList();
            IndexPostViewModel model = new IndexPostViewModel()
            {
                FullListPost = postList
            };
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));

            var prevPage = TempData["prevPage"] == null ? 0 : (int)TempData["prevPage"];
            var skip = pageSize * (page - 1);
            var canPage = skip < postList.Count;
            page = page == 0 ? (prevPage == 0 ? 1 : prevPage) : page;
            if (canPage)
            {
                postList = postList.Skip(skip).Take(pageSize).ToList();
            }
            model.ListPost = postViewModelList;
            model.PageSize = pageSize;
            model.CurrentPage = page;
            TempData["prevPage"] = prevPage;
            return View(model);
        }

        // GET: TeacherAsk
        [Route("TeacherAsk")]
        [AllowAnonymous]
        public ActionResult TeacherAsk(int page)
        {
            var pageSize = 10;
            List<Post> postList = _post
                .Where(i => i.Category.Name.Equals(ConstPostCategory.TeacherAsk) && i.Status == (int)StatusEnum.Active)
                .OrderByDescending(i => i.CreatedAt).ToList();
            IndexPostViewModel model = new IndexPostViewModel()
            {
                FullListPost = postList
            };
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            postList.ForEach(i => postViewModelList.Add(new PostViewModel(i)));

            var prevPage = TempData["prevPage"] == null ? 0 : (int)TempData["prevPage"];
            var skip = pageSize * (page - 1);
            var canPage = skip < postList.Count;
            page = page == 0 ? (prevPage == 0 ? 1 : prevPage) : page;
            if (canPage)
            {
                postList = postList.Skip(skip).Take(pageSize).ToList();
            }
            model.ListPost = postViewModelList;
            model.PageSize = pageSize;
            model.CurrentPage = page;
            TempData["prevPage"] = prevPage;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddComment(IFormCollection model)
        {
            string comment = model["NewComment"].ToString();
            short postId = 0;
            if (Int16.TryParse(model["Id"], out postId))
            {
                Comment newComment = new Comment()
                {
                    PostId = postId,
                    Content = comment,
                    CreatedAt = DateTime.Now,
                    CreatedBy = GetUserId()
                };
                _comment.Add(newComment);
            }
            List<Comment> commentList = new List<Comment>();
            commentList = _post.FindIncludeComments(postId).Comments.OrderByDescending(c => c.CreatedAt).ToList();
            return PartialView("CommentListPartial", commentList);
        }

        [HttpPost]
        public ActionResult RemoveComment(int id, int postId)
        {
            Comment removeComment = _comment.Find(id);
            _comment.RemoveWhere(i => i.Id == id);
            List<Comment> commentList = new List<Comment>();
            commentList = _post.FindIncludeComments(postId).Comments.OrderByDescending(c => c.CreatedAt).ToList();
            return PartialView("CommentListPartial", commentList);
        }
    }
}