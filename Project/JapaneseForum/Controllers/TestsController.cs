﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using JapaneseForum.Models.TestViewModels;
using JapaneseForum.Repositories;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using JapaneseForum.Classes;
using Microsoft.Extensions.Logging;
using JapaneseForum.Const;
using JapaneseForum.Authorization;

namespace JapaneseForum.Controllers
{
    [Authorize]
    public class TestsController : BaseController
    {
        private ITestRepository _testRepository;
        private IQuestionRepository _questionRepository;
        private IQuestionTypeRepository _questionTypeRepository;
        private ITestAnswerRepository _testAnswerReponsitory;
        private IAuthorizationService _authorizationService;
        private ITestPermissionRepository _testPerRepository;
        private readonly ILogger _logger;

        public TestsController(
            ITestRepository testRepository,
            IQuestionRepository questionRepository,
            IQuestionTypeRepository questionTypeRepository,
            ITestAnswerRepository testAnswerReponsitory,
            ITestPermissionRepository testPerRepository,
            UserManager<ApplicationUser> usermanager,
            ILoggerFactory loggerFactory,
            IAuthorizationService authorizationService) : base(usermanager)
        {
            _testRepository = testRepository;
            _questionRepository = questionRepository;
            _questionTypeRepository = questionTypeRepository;
            _testAnswerReponsitory = testAnswerReponsitory;
            _testPerRepository = testPerRepository;
            _userManager = usermanager;
            _logger = loggerFactory.CreateLogger<TestsController>();
            _authorizationService = authorizationService;
        }

        // GET: Tests
        public ActionResult Index()
        {
            ListTestViewModel listTestVm = new ListTestViewModel();
            var allTests = _testRepository.All();
            allTests.ToList().ForEach(i => listTestVm.ListTest.Add(new TestViewModel(i)));
            return View(listTestVm);
        }

        [Authorize(Roles ="Teacher")]
        // GET: Tests/Create
        public ActionResult Create()
        {
            CreateTestViewModel model = new CreateTestViewModel();
            model.StatusList = new SelectList((new StatusCollection()).Collection, "Id", "Name");
            return View(model);
        }
        
        [Authorize(Roles = "Teacher")]
        // POST: Tests/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateTestViewModel model)
        {
            try
            {
                Test test = new Test()
                {
                    Name = model.TestName,
                    CreatedAt = DateTime.Now,
                    CreatedBy = GetUserId(),
                    UpdatedAt = DateTime.Now,
                    TestTime = model.TestTime,
                    Status = (int)model.Status
                };
                
                if (_testRepository.Add(test) == 1)
                {
                    TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                    return View(model);
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Create test error", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }

        // GET: Tests/Edit/5
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Edit(int id)
        {
            var test = _testRepository.Find(id);
            if (await _authorizationService.AuthorizeAsync(User, test, Operations.Update))
            {
                CreateTestViewModel model = new CreateTestViewModel();
                model.StatusList = new SelectList((new StatusCollection()).Collection, "Id", "Name");
                model.Id = id;
                model.Status = (StatusEnum)test.Status;
                model.TestName = test.Name;
                model.TestTime = test.TestTime;
                return View(model);
            }
            return Forbid();
        }

        // POST: Tests/Edit/5
        [Authorize(Roles = "Teacher")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CreateTestViewModel model)
        {
            try
            {
                Test test = _testRepository.Find(id);
                if (test != null)
                {
                    test.Name = model.TestName;
                    test.Status = (int)model.Status;
                    test.UpdatedAt = DateTime.Now;
                    test.UpdatedBy = GetUserId();
                    test.TestTime = model.TestTime;
                    
                    if (_testRepository.Update(test) == 1)
                    {
                        TempData[ConstMessage.MESSAGE_SUCCESS_KEY] = ConstMessage.MESSAGE_SUCCESS_ADD_DATA;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                        return View(model);
                    }
                }
                else
                {
                    TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.LogError("Update test error", e);
                TempData[ConstMessage.MESSAGE_ERROR_KEY] = ConstMessage.MESSAGE_FAILED_ADD_DATA;
                return View();
            }
        }

        // GET: Tests/Delete/5
        [Authorize(Roles = "Teacher")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Tests/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Add question for the test
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Teacher")]
        public ActionResult AddQuestions()
        {
            try
            {
                TempData["_tempQuestionList"] = null;
                TempData["_tempCurrentKey"] = 0;
                TempData["_tempCurrentTest"] = 0;
                return RedirectToAction("Index", "Questions");
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> DoTest(int testId)
        {
            DoTestViewModel model = new DoTestViewModel();
            try
            {
                Test test = _testRepository.Find(testId);
                TestPermission testPer = _testPerRepository.FirstOrDefault(i => i.UserId == GetUserId() && i.TestId == testId);
                if (test != null)
                {
                    var isAuthorized = await _authorizationService.AuthorizeAsync(User, test, Operations.DoTest);
                    if (isAuthorized)
                    {
                        #region these code for not reset time when user refresh page
                        var currentTest = TempData["currentTest"];
                        if (currentTest != null && (int)currentTest == testId)
                        {
                            var testTime = TempData["TestTime"];
                            DateTime testTimeDT;
                            if (testTime != null && DateTime.TryParse(testTime.ToString(), out testTimeDT) && DateTime.Compare(testTimeDT, DateTime.Now) < 0)
                            {
                                TempData["TestTime"] = testTime.ToString();
                                return RedirectToAction("TestResult", new { testId = testId });
                            }
                            ViewBag.Time = testTime.ToString();
                            TempData["TestTime"] = testTime.ToString();
                        }
                        else
                        {
                            ViewBag.Time = DateTime.Now.AddMinutes(test.TestTime).ToString();
                            TempData["TestTime"] = DateTime.Now.AddMinutes(test.TestTime).ToString();
                        }
                        TempData["currentTest"] = testId;
                        #endregion
                        model.TestId = testId;
                        model.TestName = test.Name;
                        model.TestTime = test.TestTime;
                        List<QuestionTypeViewModel> questionTypeViewModelList = new List<QuestionTypeViewModel>();
                        QuestionTypeViewModel questionTypeViewModel = new QuestionTypeViewModel();
                        List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
                        List<Question> questions = new List<Question>();
                        List<QuestionType> questionTypeList = _questionTypeRepository.All().ToList();
                        int no = 0;
                        foreach (var type in questionTypeList)
                        {
                            questions = type.Questions.Where(i => i.TestId == testId).ToList();
                            if (questions.Count() > 0)
                            {
                                no = 0;
                                questionTypeViewModel = new QuestionTypeViewModel();
                                questionViewModelList = new List<QuestionViewModel>();
                                questionTypeViewModel.TypeName = type.Name;
                                foreach (var question in questions)
                                {
                                    questionViewModelList.Add(new QuestionViewModel
                                    {
                                        Id = question.Id,
                                        AnswerA = question.AnswerA,
                                        AnswerB = question.AnswerB,
                                        AnswerC = question.AnswerC,
                                        AnswerD = question.AnswerD,
                                        Content = question.Content,
                                        No = ++no,
                                    });
                                }
                                questionTypeViewModel.QuestionList = questionViewModelList;
                                questionTypeViewModelList.Add(questionTypeViewModel);
                            }
                        }
                        model.QuestionTypeList = questionTypeViewModelList;
                        return View(model);
                    }
                    else
                    {
                        return Forbid();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Open DoTest Error", e.Message);
                ModelState.AddModelError(string.Empty, "Xảy ra lỗi vui lòng vào lại !");
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult DoTest(DoTestViewModel model)
        {
            DateTime testTime;
            try
            {
                string testTimeStr = TempData["TestTime"].ToString();
                if (DateTime.TryParse(testTimeStr, out testTime) && DateTime.Compare(testTime, DateTime.Now) < 0)
                {
                    SaveTestResult(model);
                    SaveTestPermission(model.TestId);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        SaveTestResult(model);
                        TempData["TestTime"] = DateTime.Now;
                        SaveTestPermission(model.TestId);
                    }
                    else
                    {
                        TempData["TestTime"] = testTimeStr;
                        ViewBag.Time = testTimeStr;
                        return View(model);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError("Do Test Error : ", e.Message);
            }
            return RedirectToAction("TestResult", new { testId = model.TestId });
        }

        public ActionResult TestResult(int testId)
        {
            DoTestViewModel model = new DoTestViewModel();
            Test test = _testRepository.Find(testId);
            if (test != null)
            {
                model.TestId = testId;
                model.TestName = test.Name;
                List<QuestionTypeViewModel> questionTypeViewModelList = new List<QuestionTypeViewModel>();
                QuestionTypeViewModel questionTypeViewModel = new QuestionTypeViewModel();
                List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
                List<Question> questions = new List<Question>();
                List<QuestionType> questionTypeList = _questionTypeRepository.All().ToList();
                int no = 0;
                int mark = 0;
                int questionNo = 0;
                foreach (var type in questionTypeList)
                {
                    questions = type.Questions.Where(i => i.TestId == testId && i.TypeId == type.Id).ToList();
                    if (questions.Count() > 0)
                    {
                        no = 0;
                        questionTypeViewModel = new QuestionTypeViewModel();
                        questionViewModelList = new List<QuestionViewModel>();
                        questionTypeViewModel.TypeName = type.Name;
                        foreach (var question in questions)
                        {
                            var testAnswer = _testAnswerReponsitory.Where(m => m.QuestionId == question.Id && m.UserId == GetUserId()).FirstOrDefault();
                            if (testAnswer != null && question.RightAnswer == testAnswer.Answer)
                                mark++;
                            questionViewModelList.Add(new QuestionViewModel
                            {
                                Id = question.Id,
                                No = ++no,
                                RightAnswer = question.RightAnswer,
                                ChoosenAnswer = testAnswer == null ? 0 : testAnswer.Answer,
                                AnswerA = question.AnswerA,
                                AnswerB = question.AnswerB,
                                AnswerC = question.AnswerC,
                                AnswerD = question.AnswerD,
                                Content = question.Content
                            });
                            questionNo++;
                        }
                        questionTypeViewModel.QuestionList = questionViewModelList;
                        questionTypeViewModelList.Add(questionTypeViewModel);
                    }
                }
                model.Mark = mark;
                model.QuestionTypeList = questionTypeViewModelList;
                ViewBag.QuestionNo = questionNo;
                return View(model);
            }
            else
                return NotFound();
        }

        private void SaveTestResult(DoTestViewModel model)
        {
            int result = 0;
            var currentUser = GetUserId();
            foreach (var type in model.QuestionTypeList)
            {
                foreach (var question in type.QuestionList)
                {
                    var questionExist = _testAnswerReponsitory.Where(i => i.UserId == currentUser && i.QuestionId == question.Id).FirstOrDefault();
                    if (questionExist != null)
                    {
                        questionExist.Answer = question.ChoosenAnswer ?? 0;
                        result = _testAnswerReponsitory.Update(questionExist);
                    }
                    else
                    {
                        TestAnswer testAnswer = new TestAnswer()
                        {
                            UserId = currentUser,
                            QuestionId = question.Id,
                            Answer = (int)question.ChoosenAnswer
                        };
                        result = _testAnswerReponsitory.Add(testAnswer);
                    }
                }
            }
        }

        private void SaveTestPermission(int testId)
        {
            TestPermission testPer = _testPerRepository.FirstOrDefault(i => i.UserId == GetUserId() && i.TestId == testId);
            if (testPer != null)
            {
                testPer.IsAllowed = false;
                _testPerRepository.Update(testPer);
            }
            else
            {
                testPer = new TestPermission
                {
                    TestId = testId,
                    UserId = GetUserId(),
                    IsAllowed = false
                };
                _testPerRepository.Add(testPer);
            }
        }
    }
}