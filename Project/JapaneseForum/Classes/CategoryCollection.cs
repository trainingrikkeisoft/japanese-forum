﻿using JapaneseForum.Controllers;
using JapaneseForum.Models;
using JapaneseForum.Responsitory;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Classes
{
    public class CategoryCollection
    {
        private SelectList _collection;
        public CategoryCollection()
        {
            CreateCollection();
        }
        public SelectList Collection
        {
            get
            {
                return _collection;
            }
        }

        private void CreateCollection()
        {
        }
    }
}
