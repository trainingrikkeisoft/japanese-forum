﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Classes
{
    public class SelectItemList
    {
        public SelectList selectList;

        public SelectItemList(IEnumerable<dynamic> objList)
        {
            this.selectList = new SelectList(objList, "ID", "Name");
        }
    }
}
