﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Const;

namespace JapaneseForum.Classes
{
    public class AnswerItem
    {
        public AnswerEnum Id { get; set; }

        public string Name { get; set; }
    }
}
