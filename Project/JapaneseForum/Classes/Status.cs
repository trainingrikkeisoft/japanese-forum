﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Const;

namespace JapaneseForum.Classes
{
    public class StatusItem
    {
        public StatusEnum Id { get; set; }

        public string Name { get; set; }
    }
}
