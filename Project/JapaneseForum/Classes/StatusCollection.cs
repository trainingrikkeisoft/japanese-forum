﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Const;

namespace JapaneseForum.Classes
{
    public class StatusCollection
    {
        private List<StatusItem> _collection;

        public StatusCollection()
        {
            _collection = new List<StatusItem>();
            CreateCollection();
        }
        public List<StatusItem> Collection
        {
            get
            {
                return _collection;
            }
        }

        private void CreateCollection()
        {
            _collection.Add(new StatusItem { Id = StatusEnum.Draft, Name = "Nháp" });
            _collection.Add(new StatusItem { Id = StatusEnum.Active, Name = "Kích hoạt" });
            _collection.Add(new StatusItem { Id = StatusEnum.InActive, Name = "Không kích hoạt" });
        }
    }
}
