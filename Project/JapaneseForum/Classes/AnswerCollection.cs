﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Const;

namespace JapaneseForum.Classes
{
    public class AnswerCollection
    {
        private List<AnswerItem> _collection;

        public AnswerCollection()
        {
            _collection = new List<AnswerItem>();
            CreateCollection();
        }
        public List<AnswerItem> Collection
        {
            get
            {
                return _collection;
            }
        }

        private void CreateCollection()
        {
            _collection.Add(new AnswerItem { Id = AnswerEnum.A, Name = "Đáp án A" });
            _collection.Add(new AnswerItem { Id = AnswerEnum.B, Name = "Đáp án B" });
            _collection.Add(new AnswerItem { Id = AnswerEnum.C, Name = "Đáp án C" });
            _collection.Add(new AnswerItem { Id = AnswerEnum.D, Name = "Đáp án D" });
        }
    }
}
