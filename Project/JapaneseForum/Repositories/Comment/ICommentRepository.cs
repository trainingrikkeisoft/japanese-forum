﻿using JapaneseForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Repositories
{
    public interface ICommentRepository : IRepository<Comment>
    {
    }
}
