﻿using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace JapaneseForum.Repositories
{
    public class TestAnswerRepository : Repository<TestAnswer>, ITestAnswerRepository
    {
        public TestAnswerRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
