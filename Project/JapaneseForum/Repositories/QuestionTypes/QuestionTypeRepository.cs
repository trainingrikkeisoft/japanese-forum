﻿using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Repositories
{
    public class QuestionTypeRepository : Repository<QuestionType>, IQuestionTypeRepository
    {
        private new ApplicationDbContext _dbContext;

        public QuestionTypeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public new IEnumerable<QuestionType> All()
        {
            return _dbContext.QuestionTypes
            .Include(qt => qt.Questions);
        }
    }
}
