﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Data;
using Microsoft.EntityFrameworkCore;

namespace JapaneseForum.Repositories.Class
{
    public class ClassRepository : Repository<JapaneseForum.Models.Class>, IClassRepository
    {
        private readonly ApplicationDbContext _db;
        public ClassRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _db = dbContext;
        }

        public new IEnumerable<Models.Class> All()
        {
            return _db.Classes
                .Include(c => c.ClassStudents)
                    .ThenInclude(c => c.Student)
                .Include(c => c.CreatedOwner)
                .Include(c => c.UpdatedOwner)
                .Include(c => c.Teacher)
                .ToList();
        }

        public new Models.Class Find(params object[] keyValues)
        {
            string id = keyValues[0].ToString();
            return _dbContext.Classes
                .Include(i => i.ClassStudents)
                .Where(i => i.Id.Equals(id))
                .SingleOrDefault();
        }
    }
}
