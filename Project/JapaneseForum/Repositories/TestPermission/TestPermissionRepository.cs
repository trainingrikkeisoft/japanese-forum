﻿using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace JapaneseForum.Repositories
{
    public class TestPermissionRepository : Repository<TestPermission>, ITestPermissionRepository
    {
        public TestPermissionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
