﻿using JapaneseForum.Models;
using JapaneseForum.Data;

namespace JapaneseForum.Repositories
{
    public class TimeTableRepository : Repository<TimeTable>, ITimeTableRepository
    {
        public TimeTableRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
