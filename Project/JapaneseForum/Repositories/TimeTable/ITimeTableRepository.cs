﻿using JapaneseForum.Models;

namespace JapaneseForum.Repositories
{
    public interface ITimeTableRepository : IRepository<TimeTable>
    {
    }
}
