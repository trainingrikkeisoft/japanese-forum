﻿using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Repositories
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        private new ApplicationDbContext _dbContext;

        public PostRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public new IEnumerable<Post> All()
        {
            return _dbContext.Posts
            .Include(post => post.CreatedOwner)
            .Include(post => post.UpdatedOwner)
            .Include(post => post.Category);
        }

        public new Post Find(params object[] keyValues)
        {
            int id = (int)keyValues[0];
            return _dbContext.Posts
                .Include(i => i.CreatedOwner)
                .Include(i => i.UpdatedOwner)
                .SingleOrDefault(x => x.Id == id);
        }
        
        public Post FindIncludeComments(int id)
        {
            return _dbContext.Posts
                .Include(i => i.Comments)
                .ThenInclude(c => c.CreatedOwner)
                .Include(i => i.CreatedOwner)
                .Include(i => i.UpdatedOwner)
                .SingleOrDefault(x => x.Id == id);
        }
    }
}
