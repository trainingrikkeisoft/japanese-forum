﻿using JapaneseForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Data;

namespace JapaneseForum.Repositories
{
    public class UserRepository : Repository<ApplicationUser>, IUserRepository
    {
        public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
