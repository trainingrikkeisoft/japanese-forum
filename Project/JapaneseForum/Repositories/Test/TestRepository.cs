﻿using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Repositories
{
    public class TestRepository : Repository<Test>, ITestRepository
    {
        private new ApplicationDbContext _dbContext;

        public TestRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public new IEnumerable<Test> All()
        {
            return _dbContext.Tests
            .Include(test => test.CreatedOwner)
            .Include(test => test.UpdatedOwner);
        }
    }
}
