﻿using JapaneseForum.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace JapaneseForum.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly ApplicationDbContext _dbContext;

        public Repository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int AddOrUpdate(IEnumerable<TEntity> entities)
        {
            throw new NotImplementedException();
        }

        public int AddRange(IEnumerable<TEntity> entities)
        {
            _dbContext.Set<TEntity>().AddRange(entities);
            return _dbContext.SaveChanges();
        }

        public int Add(TEntity t)
        {
            _dbContext.Set<TEntity>().Add(t);
            return _dbContext.SaveChanges();
        }

        public int Delete(TEntity t)
        {
            _dbContext.Set<TEntity>().Remove(t);
            return _dbContext.SaveChanges();
        }

        public TEntity Find(params object[] keyValues)
        {
            return _dbContext.Set<TEntity>().Find(keyValues);
        }

        public IEnumerable<TEntity> All()
        {
            return _dbContext.Set<TEntity>().ToList();
        }

        public int RemoveRange(IEnumerable<TEntity> entities)
        {
            entities.ToList().ForEach(t => _dbContext.Set<TEntity>().Remove(t));
            return _dbContext.SaveChanges();
        }

        public int RemoveWhere(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> entities = _dbContext.Set<TEntity>().Where(predicate);
            entities.ToList().ForEach(t => _dbContext.Set<TEntity>().Remove(t));
            return _dbContext.SaveChanges();
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().SingleOrDefault(predicate);
        }

        public TEntity SingleOrDefault()
        {
            return _dbContext.Set<TEntity>().SingleOrDefault();
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().FirstOrDefault(predicate);
        }

        public TEntity FirstOrDefault()
        {
            return _dbContext.Set<TEntity>().FirstOrDefault();
        }

        public int Update(TEntity t)
        {
            _dbContext.Set<TEntity>().Update(t);
            return _dbContext.SaveChanges();
        }

        public int UpdateRange(IEnumerable<TEntity> entities)
        {
            entities.ToList().ForEach(t => _dbContext.Set<TEntity>().Update(t));
            return _dbContext.SaveChanges();
        }

        public IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().Where(predicate);
        }
    }
}
