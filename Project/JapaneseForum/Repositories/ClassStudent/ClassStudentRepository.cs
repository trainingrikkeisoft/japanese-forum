﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JapaneseForum.Data;

namespace JapaneseForum.Repositories.ClassStudent
{
    public class ClassStudentRepository : Repository<Models.ClassStudent>, IClassStudentRepository
    {
        public ClassStudentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
