﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Repositories.ClassStudent
{
    public interface IClassStudentRepository : IRepository<Models.ClassStudent>
    {
    }
}
