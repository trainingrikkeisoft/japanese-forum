﻿using JapaneseForum.Models;
using JapaneseForum.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Repositories
{
    public interface IFineRepository : IRepository<Fine>
    {

    }
}
