﻿using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace JapaneseForum.Repositories
{
    public class FineRepository : Repository<Fine>, IFineRepository
    {
        private readonly ApplicationDbContext _db;

        public FineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _db = dbContext;
        }

        public new IEnumerable<Fine> Where(Expression<Func<Fine, bool>> predicate)
        {
            return _db.Fines.Include(c => c.Student).Where(predicate);
        }

    }
}
