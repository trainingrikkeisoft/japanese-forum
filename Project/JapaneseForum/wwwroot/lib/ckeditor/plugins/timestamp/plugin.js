﻿CKEDITOR.plugins.add('timestamp', {

    // Register the icons. They must match command names.
    icons: 'timestamp',

    // The plugin initialization logic goes inside this method.
    init: function (editor) {

        // Define the editor command that inserts a timestamp.
        editor.addCommand('insertTimestamp', {

            // Define the function that will be fired when the command is executed.
            exec: function (editor) {
                window.open(window.location.origin + '/FileUpload/AudioUpload', null,
                    "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
            }
        });

        // Create the toolbar button that executes the above command.
        editor.ui.addButton('Timestamp', {
            label: 'Insert Timestamp',
            command: 'insertTimestamp',
            toolbar: 'insert'
        });
    }
});