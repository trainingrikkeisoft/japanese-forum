﻿// const
var REPEAT_DAILY = 1;
var REPEAT_WEEKLY = 2;
var REPEAT_MONTHLY = 3;
var REPEAT_YEARLY = 4;

// Write your Javascript code.
function turnOnValidateCKEditor(item) {
    CKEDITOR.on('instanceReady', function () {
        $(item).removeAttr('style');
        $(item).css({ "visibility": "hidden", "height": "0" });
    });
}

// calendar
function calendarGenerate(currentCalendar, getEventsUrl, eventSelectedEvent, eventClickedEvent) {
    $(currentCalendar).fullCalendar({
        defaultDate: moment(),
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        select: eventSelectedEvent,
        defaultView: 'month',
        events: getEventsUrl,
        eventRender: function (event, element, view) {
            if (event.ranges == null)
                return null;
            return event.ranges.filter(function (range) {
                return event.start.isBefore(range.end) &&
                    event.end.isAfter(range.start);
            }).length > 0;
        },
        eventClick: eventClickedEvent,
        dayRender: function (date, cell) {
            if (date < date) {
                $(cell).addClass('disabled');
            }
        }
    });
}

function deleteEvent(id) {
    currentCalendar.fullCalendar('removeEvents', id);
}


//end calendar