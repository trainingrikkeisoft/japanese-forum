﻿using JapaneseForum.Const;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Threading.Tasks;

namespace JapaneseForum.Services
{
    public class RoleInitialize
    {
        public static async Task Initialize(RoleManager<IdentityRole> roleManager)
        {
            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_ADMIN))
            {
                var role = new IdentityRole(ConstAuthozation.ROLE_ADMIN);
                await roleManager.CreateAsync(role);
            }
            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_TEACHER))
            {
                var role = new IdentityRole(ConstAuthozation.ROLE_TEACHER);
                await roleManager.CreateAsync(role);
            }
            if (!await roleManager.RoleExistsAsync(ConstAuthozation.ROLE_PRESIDENT))
            {
                var role = new IdentityRole(ConstAuthozation.ROLE_PRESIDENT);
                await roleManager.CreateAsync(role);
            }
        }
    }
}
