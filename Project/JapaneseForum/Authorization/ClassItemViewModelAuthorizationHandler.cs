﻿using JapaneseForum.Const;
using JapaneseForum.Data;
using JapaneseForum.Models;
using JapaneseForum.Models.ClassViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Authorization
{
    public class ClassItemViewModelAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, ClassItemViewModel>
    {
        UserManager<ApplicationUser> _userManager;
        ApplicationDbContext _db;

        public ClassItemViewModelAuthorizationHandler(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, ClassItemViewModel resource)
        {
            if (context.User == null || resource == null)
            {
                return Task.FromResult(0);
            }

            if (requirement.Name == ConstOperation.ViewFineFromClassOperationName)
            {
                if (context.User.IsInRole(ConstAuthozation.ROLE_ADMIN))
                {
                    context.Succeed(requirement);
                }
                else if (context.User.IsInRole(ConstAuthozation.ROLE_PRESIDENT) 
                    && resource.ClassStudents.Count > 0 
                    && resource.ClassStudents.Select(i => i.Student.Id).Contains(_userManager.GetUserId(context.User))){
                    context.Succeed(requirement);
                }
                else if (context.User.IsInRole(ConstAuthozation.ROLE_TEACHER) 
                    && resource.TeacherId.Equals(_userManager.GetUserId(context.User)))
                {
                    context.Succeed(requirement);
                }
            }

            return Task.FromResult(0);
        }
    }
}
