﻿using JapaneseForum.Const;
using JapaneseForum.Data;
using JapaneseForum.Models;
using JapaneseForum.Models.PostViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Authorization
{
    public class PostViewModelAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, PostViewModel>
    {
        UserManager<ApplicationUser> _userManager;
        ApplicationDbContext _db;

        public PostViewModelAuthorizationHandler(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, PostViewModel resource)
        {
            if (context.User == null || resource == null)
            {
                return Task.FromResult(0);
            }

            if ((requirement.Name == ConstOperation.UpdateOperationName ||
                requirement.Name == ConstOperation.DeleteOperationName)
                &&
                (resource.CreatedBy == _userManager.GetUserId(context.User) 
                || context.User.IsInRole(ConstAuthozation.ROLE_ADMIN)))
            {
                context.Succeed(requirement);
            }

            if (requirement.Name == ConstOperation.ViewItemOperationName
                && (resource.CreatedBy == _userManager.GetUserId(context.User)
                || context.User.IsInRole(ConstAuthozation.ROLE_ADMIN) || resource.Status == (int)StatusEnum.Active))
            {
                context.Succeed(requirement);
            }

            return Task.FromResult(0);
        }
    }
}
