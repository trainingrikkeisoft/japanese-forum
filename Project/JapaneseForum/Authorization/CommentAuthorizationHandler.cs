﻿using JapaneseForum.Const;
using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Authorization
{
    public class CommentAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Comment>
    {
        UserManager<ApplicationUser> _userManager;
        ApplicationDbContext _db;

        public CommentAuthorizationHandler(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, Comment resource)
        {
            if (context.User == null || resource == null)
            {
                return Task.FromResult(0);
            }

            if ((requirement.Name == ConstOperation.UpdateOperationName ||
                requirement.Name == ConstOperation.DeleteOperationName)
                &&
                (resource.CreatedBy == _userManager.GetUserId(context.User) 
                || context.User.IsInRole(ConstAuthozation.ROLE_ADMIN)))
            {
                context.Succeed(requirement);
            }

            return Task.FromResult(0);
        }
    }
}
