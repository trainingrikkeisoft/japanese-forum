﻿using JapaneseForum.Const;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace JapaneseForum.Authorization
{
    public static class Operations
    {
        public static OperationAuthorizationRequirement Create =
          new OperationAuthorizationRequirement { Name = ConstOperation.CreateOperationName };
        public static OperationAuthorizationRequirement ViewDetail =
          new OperationAuthorizationRequirement { Name = ConstOperation.ViewDetailOperationName };
        public static OperationAuthorizationRequirement ViewItem =
          new OperationAuthorizationRequirement { Name = ConstOperation.ViewItemOperationName };
        public static OperationAuthorizationRequirement Update =
          new OperationAuthorizationRequirement { Name = ConstOperation.UpdateOperationName };
        public static OperationAuthorizationRequirement Delete =
          new OperationAuthorizationRequirement { Name = ConstOperation.DeleteOperationName };
        public static OperationAuthorizationRequirement DoTest =
          new OperationAuthorizationRequirement { Name = ConstOperation.DoTestOperationName };
        public static OperationAuthorizationRequirement ViewFineFromClass =
          new OperationAuthorizationRequirement { Name = ConstOperation.ViewFineFromClassOperationName };
    }
}