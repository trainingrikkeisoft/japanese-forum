﻿using JapaneseForum.Const;
using JapaneseForum.Data;
using JapaneseForum.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Authorization
{
    public class TestAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Test>
    {
        UserManager<ApplicationUser> _userManager;
        ApplicationDbContext _db;

        public TestAuthorizationHandler(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, Test resource)
        {
            if (context.User == null || resource == null)
            {
                return Task.FromResult(0);
            }

            if (requirement.Name == ConstOperation.UpdateOperationName &&
                requirement.Name == ConstOperation.DeleteOperationName &&
                resource.CreatedBy == _userManager.GetUserId(context.User))
            {
                context.Succeed(requirement);
            }

            if (requirement.Name == ConstOperation.DoTestOperationName)
            {
                var userId = _userManager.GetUserId(context.User);
                var testId = resource.Id;
                var testPer = _db.TestPermissions.FirstOrDefault(i => i.TestId == testId && i.UserId == userId);
                if (testPer == null || testPer.IsAllowed == true)
                    context.Succeed(requirement);
            }

            if (requirement.Name != ConstOperation.CreateOperationName &&
                (context.User.IsInRole(ConstAuthozation.ROLE_ADMIN)
                || context.User.IsInRole(ConstAuthozation.ROLE_TEACHER)))
            {
                context.Succeed(requirement);
            }

            return Task.FromResult(0);
        }
    }
}
