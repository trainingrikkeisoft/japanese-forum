﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using JapaneseForum.Data;
using JapaneseForum.Models;
using JapaneseForum.Services;
using Microsoft.AspNetCore.Identity;
using JapaneseForum.Repositories;
using JapaneseForum.Classes;
using JapaneseForum.Repositories.Class;
using JapaneseForum.Repositories.ClassStudent;
using Microsoft.AspNetCore.Authorization;
using JapaneseForum.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using JapaneseForum.Const;
using System.IO;

namespace JapaneseForum
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            ConstConfiguration.DefaultThumbnail = Configuration["DefaultThumbnail"];

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.AddMemoryCache();
            services.AddSession(/* options go here */);

            services.AddMvc();

            services.AddSingleton<IConfiguration>(Configuration);

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            //Authorization
            services.AddScoped<IAuthorizationHandler, TestAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, TestViewModelAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, PostAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, PostViewModelAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, ClassItemViewModelAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, CommentAuthorizationHandler>();

            //DB Respository
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IPostRepository, PostRepository>();
            services.AddTransient<ITestRepository, TestRepository>();
            services.AddTransient<IQuestionRepository, QuestionRepository>();
            services.AddTransient<IQuestionTypeRepository, QuestionTypeRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IClassRepository, ClassRepository>();
            services.AddTransient<IClassStudentRepository, ClassStudentRepository>();
            services.AddTransient<ITestAnswerRepository, TestAnswerRepository>();
            services.AddTransient<ITestPermissionRepository, TestPermissionRepository>();
            services.AddTransient<ILaborRepository, LaborRepository>();
            services.AddTransient<ITimeTableRepository, TimeTableRepository>();
            services.AddTransient<IFineRepository, FineRepository>();
            services.AddTransient<ICommentRepository, CommentRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, RoleManager<IdentityRole> roleManager)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("Logs/JapaneseForum-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseSession();
            app.UseStaticFiles();
            app.UseIdentity();

            //Seed data
            var adminMail = Configuration["AdminMail"];
            SeedData.InitializeAsync(app.ApplicationServices, adminMail).Wait();

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            app.UseGoogleAuthentication(new GoogleOptions()
            {
                ClientId = Configuration["Authentication:Google:ClientId"],
                ClientSecret = Configuration["Authentication:Google:ClientSecret"],
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            await RoleInitialize.Initialize(roleManager);


            Directory.CreateDirectory("wwwroot/uploads");
        }
    }
}
