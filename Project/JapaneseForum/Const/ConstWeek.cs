﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstWeek
    {
        public static int Sunday = 0;
        public static int Monday = 1;
        public static int Tuesday = 2;
        public static int Wednesday = 3;
        public static int Thursday = 4;
        public static int Friday = 5;
        public static int Saturday = 6;
    }
}
