﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstGender
    {
        public static int Unknow = 0;
        public static int Male = 1;
        public static int Female = 2;
    }
}
