﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstMessage
    {
        public static readonly string MESSAGE_ERROR_KEY = "MESSAGE_ERROR";
        public static readonly string MESSAGE_WARNING_KEY = "MESSAGE_WARNING";
        public static readonly string MESSAGE_SUCCESS_KEY = "MESSAGE_SUCCESS";

        public static readonly string MESSAGE_ERROR_EMAIL_EXITS = "Email đã tồn tại";
        public static readonly string MESSAGE_ERROR_DATA_NOT_EXITS = "Dữ liệu không tồn tại";

        public static readonly string MESSAGE_SUCCESS_ADD_DATA = "Thêm mới dữ liệu thành công";
        public static readonly string MESSAGE_SUCCESS_DELETE_DATA = "Xóa dữ liệu thành công";
        public static readonly string MESSAGE_SUCCESS_UPDATE_DATA = "Cập nhật dữ liệu thành công";

        public static readonly string MESSAGE_FAILED_ADD_DATA = "Thêm mới dữ liệu thất bại";
        public static readonly string MESSAGE_FAILED_DELETE_DATA = "Xóa dữ liệu thất bại";
        public static readonly string MESSAGE_FAILED_UPDATE_DATA = "Cập nhật dữ liệu thất bại";
    }
}
