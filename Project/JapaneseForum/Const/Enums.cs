﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public enum MediaTypeEnum
    {
        Audio = 1,
        Video = 2,
        Image = 3
    }
    public enum StatusEnum
    {
        Draft = 1,
        Active = 2,
        InActive = 3
    }

    public enum AnswerEnum
    {
        A = 1,
        B = 2,
        C = 3,
        D = 4
    }

    public enum SortOrder
    {
        None,
        Id,
        Name,
        Category,
        Status,
        CreatedAt,
        UpdatedAt,
        CreatedBy,
        UpdatedBy
    }
}
