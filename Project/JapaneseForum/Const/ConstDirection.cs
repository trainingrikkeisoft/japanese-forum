﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstDirection
    {
        public static string PostDirection = "/Post";
        public static string TestsDirection = "/Tests";
        public static string LaborDirection = "/Labor";
        public static string ClassDirection = "/Class";
        public static string UserDirection = "/User";
        public static string FineDirection = "/Fine";
        public static string AboutDirection = "/About";
        public static string LearnDirection = "/Learn";
        public static string EntertainmentDirection = "/Entertainment";
        public static string TeacherAskDirection = "/TeacherAsk";
        public static string Document = "/Document";
    }
}
