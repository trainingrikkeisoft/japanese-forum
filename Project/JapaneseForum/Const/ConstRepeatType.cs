﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstRepeatType
    {
        public const int NotRepeat = 0;
        public const int Daily = 1;
        public const int Weekly = 2;
        public const int Monthly = 3;
        public const int Yearly = 4;
    }
}
