﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstOperation
    {
        public static readonly string CreateOperationName = "Create";
        public static readonly string ViewDetailOperationName = "ViewDetail";
        public static readonly string ViewItemOperationName = "ViewItem";
        public static readonly string UpdateOperationName = "Update";
        public static readonly string DeleteOperationName = "Delete";
        public static readonly string DoTestOperationName = "DoTest";
        public static readonly string ViewFineFromClassOperationName = "ViewFine";
    }
}
