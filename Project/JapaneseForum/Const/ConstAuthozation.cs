﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    public class ConstAuthozation
    {
        public static readonly string ROLE_ADMIN = "Admin";
        public static readonly string ROLE_TEACHER = "Teacher";
        public static readonly string ROLE_PRESIDENT = "President";
        public static readonly string ROLE_STUDENT = "Student";
    }
}
