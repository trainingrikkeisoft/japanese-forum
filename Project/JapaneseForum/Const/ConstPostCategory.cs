﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace JapaneseForum.Const
{
    /// <summary>
    /// Định nghĩa Category ở đây
    /// Nếu muốn thêm Category chỉ cần định nghĩa category vào đây
    /// SeedData sẽ tự lấy ở đây và thêm vào database
    /// </summary>
    public class ConstPostCategory
    {
        public static string Lesson { get => "Bài học"; }
        public static string TeacherAsk { get => "Thưa cô em hỏi"; }
        public static string EntertainmentAndNews { get => "Giải trí và tin tức"; }
        public static string Document { get => "Tài liệu học tập"; }
    }
}
